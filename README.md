= Jasmin =

A DB and API administration client written entirely in JavaScript.
Open http://localhost:46256/js/ in a browser and you get an DB client,
without any installation. It should work on modern browsers,
on desktop, tablets and smartphones.

It's single-page application (SPA). The whole client application and
user interface is completely generated in JavaScript, and even though
it's technically a webpage, it behaves like an application.

A key clue is that it's entirely generated. There is no custom code
per entity, but it generates the whole UI from the model.
The UI will of course not be as polished and adapted to each form as a
custom desktop client would be.

Advantages over certain other DB administration clients:
* Works everywhere, even on tablets and smartphones.
* No installation, no deployment hassles
* Fast. Blazingly fast.

For the moment, this is a side project by me. I had this idea in my mind
since a long time, and I wanted to explore whether it actually works,
and how far I can take it and how usable it will be.
