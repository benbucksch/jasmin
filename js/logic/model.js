/**
 * This file implements the meta-data structures about
 * types (classes, entities), and their instances.
 *
 * It also contains the code to parse the info from the server
 * (model XML files) to build this internal representation.
 *
 * The *Type classes are representations of information found in the
 * model XML files, particularly entity classes, subentites, enums etc..
 * The *Instance classes are representations of concrete entity objects.
 *
 * JasminType
 *   PrimitiveType
 *   EntityType
 *     AggregateEntityType
 *       subTypes -> EntityType
 *     SummaryType
 *       requestType -> EntityType
 *
 * EntityInstance captures an instance of an EntityType.
 * It has the functions to retrieve actual data from the server,
 * and update the server with new data.
 */

var modelExports = {
  JasminType : JasminType,
  PrimitiveType : PrimitiveType,
  booleanType : booleanType,
  integerType : integerType,
  decimalType : decimalType,
  stringType : stringType,
  dateType : dateType,
  dateTimeType : dateTimeType,
  EnumType : EnumType,
  EntityType : EntityType,
  AggregateEntityType : AggregateEntityType,
  SummaryType : SummaryType,
  EntityInstance : EntityInstance,
  AggregateEntityInstance : AggregateEntityInstance,
  SearchRequest : SearchRequest,
  JasminException : JasminException,
  makeFullName : makeFullName,
  getNamespace : getNamespace,
}

/**
 * Superclass for all types in models.
 */
function JasminType() {
}
JasminType.prototype = {
  name : null,

  /**
   * Checks whether a given JS value is of this type.
   * @override
   */
  instanceOf : function(value) {
    throw new NotImplemented();
  },

  /**
   * @param jsonValue {JSON} a raw JSON value
   * @returns {Any} a value in our JS model system here.
   * e.g. an {EntityInstance} or an EnumType option.
   */
  instanceFromJSON : function(jsonValue) {
    throw new NotImplemented();
  },

  instanceToJSON : function(value) {
    throw new NotImplemented();
  },

  /**
   * For a given value, returns a string that you can
   * display to the end user.
   * @param value {type of this.jsType}
   * @returns {String}
   */
  display : function(value) {
    throw new NotImplemented();
  },

  displayShort : function(value) {
    var str = this.display(value);
    var strShort = str.substr(0, 30);
    if (str != strShort) {
      strShort += "...";
    }
    return strShort;
  },
}

/**
 * This is an integer, decimal, string etc.
 */
function PrimitiveType(name, jsType, jsTypeName) {
  JasminType.call(this);
  this.name = sanitize.identifier(name);
  this.jsType = jsType;
  this.jsTypeName = jsTypeName;
}
PrimitiveType.prototype = {
  name : null,
  jsType : null,
  jsTypeName : null,

  /**
   * Checks whether a given JS value is of this type.
   * @override
   */
  instanceOf : function(value) {
    return typeof(value) == this.jsTypeName &&
      (this.jsTypeName != "object" || value instanceof this.jsType);
  },

  instanceFromJSON : function(jsonValue) {
    if (!jsonValue && this.name == "string") {
      return null;
    }
    assert(this.instanceOf(jsonValue), "Property value has wrong type. Expected: " +
        this.name + ", but got: " + jsonValue);
    return jsonValue;
  },

  instanceToJSON : function(value) {
    return value;
  },

  display : function(value) {
    if (value === null || value === undefined) {
      return "";
    }
    return sanitize.label(value);
  },
}
extend(PrimitiveType, JasminType);

function NumberType(name, jsType, jsTypeName) {
  PrimitiveType.call(this, name, jsType, jsTypeName);
}
NumberType.prototype = {
  instanceToJSON : function(value) {
    return value.toJSON();
  },

  display : function(number) {
    if (number === null || number === undefined) {
      return "";
    }
    return number.toLocaleString();
  },
}
extend(NumberType, PrimitiveType);

function BooleanType(name, jsType, jsTypeName) {
  PrimitiveType.call(this, name, jsType, jsTypeName);
}
BooleanType.prototype = {
  display : function(bool) {
    return bool ? "Yes" : "No";
  },
}
extend(BooleanType, PrimitiveType);

function DateType() {
  PrimitiveType.call(this, "date", Date, "object");
}
DateType.prototype = {
  instanceOf : function(value) {
    return value instanceof this.jsType;
  },

  instanceFromJSON : function(jsonValue) {
    if (!jsonValue) {
      return null;
    }
    jsonValue = sanitize.nonemptystring(jsonValue);
    return new Date(jsonValue);
  },

  instanceToJSON : function(value) {
    return value.toJSON().substr(0, 10);
  },

  display : function(date) {
    if (!date) {
      return "";
    }
    //return date.toLocaleDateString(locale, { day: "2-digit", month: "2-digit", year: "numeric", });
    if (Intl && Intl.DateTimeFormat) {
      return Intl.DateTimeFormat([], { day: "2-digit", month: "2-digit", year: "numeric", }).format(date);
    } else {
      return date.toLocaleDateString();
    }
  },
}
extend(DateType, PrimitiveType);

function DateTimeType() {
  DateType.call(this);
  this.name = "datetime";
}
DateTimeType.prototype = {
  instanceToJSON : function(value) {
    return value.toJSON();
  },

  display : function(date) {
    if (!date) {
      return "";
    }
    if (Intl && Intl.DateTimeFormat) {
      return Intl.DateTimeFormat([], { minute: "2-digit", hour: "2-digit", day: "2-digit", month: "2-digit", year: "numeric", }).format(date);
    } else {
      return date.toLocaleString();
    }
  },
}
extend(DateTimeType, DateType);

var booleanType = new BooleanType("boolean", Boolean, "boolean");
var integerType = new PrimitiveType("int", Number, "number");
var decimalType = new PrimitiveType("decimal", Number, "number");
var stringType = new PrimitiveType("string", String, "string");
var dateType = new DateType();
var dateTimeType = new DateTimeType();

/*
function NumberType() {
}
NumberType.prototype = {
}
extend(NumberType, PrimitiveType);
*/

function EnumType(fullName) {
  JasminType.call(this);
  this.fullName = sanitize.identifier(fullName);
  var nameParts = fullName.split(".");
  this.typeName = nameParts.pop();
  this.namespace = nameParts.join(".");
  this.name = this.typeName;
  this.options = {}
}
EnumType.prototype = {
  /**
   * {Map Name {String} -> enum option info {
   *   name : {String},
   *   description : {String},
   *   value : {Integer},
   * }}
   */
  options : null,

  /**
   * @see EntityType
   */
  fullName : null,

  /**
   * @see EntityType
   */
  namespace : null,

  /**
   * @see EntityType
   */
  typeName : null,

  /**
   * @see EntityType
   */
  _loaded : 0,

  fetchType : function(successCallback, errorCallback) {
    if (this._loaded == 1) {
      successCallback();
      return;
    }
    // TODO Fetch all types in the namespace at once
    fetchTypeFromServer(this.fullName, type => {
      assert(type == this);
      successCallback();
    }, ex => {
      errorCallback("Cannot fetch EnumType " + this.fullName + ", because I have no way to find enums: " + ex);
    });
  },

  instanceOf : function(option) {
    assert(typeof(option) == "object");
    return this.options[option.name] == option;
  },

  /**
   * @returns the matching |options| object.
   */
  instanceFromJSON : function(jsonValue) {
    jsonValue = sanitize.identifier(jsonValue);
    var option = this.options[jsonValue];
    assert(option, "Unknown option value for propInfo.type.name: " + jsonValue);
    return option;
  },

  instanceToJSON : function(option) {
    return option.name;
  },

  display : function(option) {
    if (!option) {
      return "";
    }
    var label = gApp.strings.getLabelForProperty(this, option.name);
    if (!label) { // fallback
      label = option.Description; // option.name?
    }
    return label;
  },
}
extend(EnumType, JasminType);

EnumType.newType = function(fullName) {
  var type = gApp._allTypes[fullName];
  if (type) {
    return type;
  }
  type = new EnumType(fullName);
  gApp._addNewType(type);
  return type;
}



/**
 * This represents an entity type that's defined in the model XML files.
 * E.g. "LeaseMain"
 *
 * Before you can use the types of the properties here, you must call
 * fetchPropertyTypes().
 */
function EntityType(fullName) {
  JasminType.call(this);
  this.fullName = sanitize.identifier(fullName);
  var nameParts = fullName.split(".");
  this.typeName = nameParts.pop();
  this.namespace = nameParts.join(".");
  this.shortName = this.typeName; // will be overwritten for subentity types
  this.name = this.typeName;
  this._propertiesMap = {};
  this._verbs = [];
}
EntityType.prototype = {
  /**
   * The name of the aggregate type, with namespace.
   * E.g. "Base.Foo.BarMain".
   * {String}
   */
  fullName : null,

  /**
   * The namespace in which this type lives. Without trailing dot.
   * E.g. "Base.Foo".
   * {String}
   */
  namespace : null,

  /**
   * The name of the aggregate type, without namespace.
   * E.g. "BarMain".
   * {String}
   */
  typeName : null,

  /**
   * The short name of the aggregate type, without namespace and
   * without the aggregate name.
   * Identical to typeName for all types that are not sub entities.
   * E.g. "Main".
   * {String}
   */
  shortName : null,

  /**
   * Name of the property that contains the primary key.
   * E.g. "Id" or "BarId"
   * {String}
   */
  keyPropertyName : null,
  keyPropertyAlias : null,

  /**
   * A list of properties that this EntityType can have, and their types.
   * The order should match the order in the model XML file.
   * Note: This is just the type. The property value is in the EntityInstance.
   *
   * {Map of property name {String} -> PropertyInfo {
   *   name : {string}
   *   type : property type {JasminType},
   *   required : true/false,
   *   Alias : like model XML {String},
   *   IsSummaryFor : like model XML {String},
   *   Access : like model XML {String},
   * }}
   */
  _propertiesMap : null,

  /**
   * List of verbs that can be called on this subject.
   * valid: "Create", "Update", "Delete", "Request", "RequestAll"
   * {Array of verb {String-Enum}}
   */
  _verbs : null,

  /**
   * If true, this is a subentity in an Aggregate,
   * and it's a 1:n relation, meaning that there is a sub
   * entity collection/list, not a single sub entity here.
   */
  is1toN : false,

  /**
   * @see fetchType() and fetchPropertyTypes()
   * {Integer-Enum}
   * 0 = This type has only the name filled, but the properties are empty
   * 1 = the properties are filled, but some of their types might be _loaded = 0.
   * 2 = the types of all properties have been loaded (each has _loaded >= 1)
   */
  _loaded : 0,

  /**
   * @returns {Array of PropInfo}
   */
  get properties() {
    return mapToArray(this._propertiesMap);
  },

  /**
   * Checks property name and property Alias.
   * @returns {PropInfo}
   *    null, if not found
   */
  getProperty : function(name) {
    if (this._propertiesMap[name]) {
      return this._propertiesMap[name];
    }
    var aliased = this.properties.filter(propInfo => {
      return propInfo.Alias && propInfo.Alias == name;
    })[0];
    if (aliased) {
      return aliased;
    }
    return null;
  },

  fetchType : function(successCallback, errorCallback) {
    if (this._loaded >= 1) {
      successCallback();
      return;
    }

    // Avoid concurrent fetches resulting in duplicated types
    if (gApp._waitingForNamespaces[this.namespace]) {
      gApp._waitingForNamespaces[this.namespace].push({
        successCallback : successCallback,
        errorCallback : errorCallback,
      });
      return;
    }
    gApp._waitingForNamespaces[this.namespace] = [];

    fetchTypeFromServer(this.fullName, type => {
      assert(type == this);
      successCallback();

      gApp._waitingForNamespaces[this.namespace].forEach(waiting => {
        try {
          waiting.successCallback();
        } catch (ex) {
          waiting.errorCallback(ex);
        }
      });
      gApp._waitingForNamespaces[this.namespace] = null;
    }, ex => {
      errorCallback(ex);
      gApp._waitingForNamespaces[this.namespace].forEach(waiting => waiting.errorCallback(ex));
      gApp._waitingForNamespaces[this.namespace] = null;
    });
  },

  /**
   * You need to call this before you can access the |type| of the |properties|.
   * Particularly for referenced (linked) EntityType and EnumType.
   *
   * We do this only on request, to avoid dragging in half of all the app APIs,
   * just for loading one type, by iterating to refed type of refed type of ... etc.
   * So, this loads only one level of depth: the EntityTypes of the properties here.
   */
  fetchPropertyTypes : function(successCallback, errorCallback) {
    if (this._loaded == 2) {
      successCallback();
    }
    var waiting = 0;
    this.properties.forEach(propInfo => {
      var type = propInfo.type;
      if ((type instanceof EntityType || type instanceof EnumType) &&
          type._loaded == 0) {
        waiting++;
        type.fetchType(() => {
          if (--waiting == 0) {
            this._loaded = 2;
            successCallback();
          }
        }, errorCallback);
      }
    });
    if (!waiting) {
      this._loaded = 2;
      successCallback();
    }
  },

  instanceOf : function(value) {
    return value instanceof EntityInstance;
  },

  /**
   * This creates a new object with a concrete instance of this type.
   * The AggregateEntityType will return an AggregateEntityInstance,
   * which makes this function more convenient than a direct |new|.
   * @returns {EntityInstance}
   */
  newInstance : function(id) {
    return new EntityInstance(this, id);
  },

  instanceFromJSON : function(jsonValue) {
    throw new NotReached(this.fullName + " is used as part of another entity. Only Summaries can. Check the model XML. Or maybe your Summary name doesn't end in *Summary.");
    // JSON to entity implementation is in EntityInstance._parseJSONInstance().
  },

  instanceToJSON : function(instance) {
    if (!instance) {
      return null;
    }
    assert(this.instanceOf(instance));
    // TODO set ID, but on the FooID field, not the Summary field
    return instance.id;
  },

  display : function(instance) {
    if (!instance) {
      return "";
    }
    return instance.title();
  },
}
extend(EntityType, JasminType);

EntityType.newType = function(fullName) {
  var type = gApp._allTypes[fullName];
  if (type) {
    return type;
  }
  type = new EntityType(fullName);
  gApp._addNewType(type);
  return type;
}


function AggregateEntityType(fullName) {
  EntityType.call(this, fullName);
  this._subTypes = {};
  this._subListTypes = {};
}
AggregateEntityType.prototype = {

  /**
   * The "Main" subentity, e.g. RequisitionGroupMain
   * {EntityType}
   */
  mainType : null,

  /**
   * All sub entity types (groups) including the main one
   * Not including the 1:n relations - these are in subListTypes.
   * {Map typeName {String} -> {EntityType}}
   */
  _subTypes : null,

  /**
   * All sub entity types with 1:n relations
   * {Map typeName {String} -> {EntityType}}
   */
  _subListTypes : null,

  /**
   * All subtypes
   * Including the main one.
   * Not including the 1:n relations - these are in subListTypes.
   * {Array of {EntityType}}
   */
  get subTypes() {
    return mapToArray(this._subTypes);
  },

  /**
   * All 1:n relations, i.e. detail tables.
   * {Array of {EntityType}}
   */
  get subListTypes() {
    return mapToArray(this._subListTypes);
  },

  getSubType : function(typeName) {
    return this._subTypes[typeName] || this.subListTypes[typeName];
  },

  /**
   * For parseModelXML()
   */
  _addSubType : function(entityType) {
    assert(entityType instanceof EntityType);
    assert(!this._subTypes[entityType.typeName] &&
           !this._subListTypes[entityType.typeName],
        entityType.typeName + " is already in the aggregate");
    if (entityType.is1toN) {
      this._subListTypes[entityType.typeName] = entityType;
    } else {
      this._subTypes[entityType.typeName] = entityType;
    }

    // Find main subtype
    var isNameMain = strEndsWith(entityType.typeName, "Main") || strEndsWith(entityType.typeName, "General");
    var hasDelete = arrayContains(entityType._verbs, "Delete");
    var isFirst = this.subTypes.length == 1;
    // If all of this hits, this is most likely the main sub entity
    if (isNameMain && hasDelete) {
      this.mainType = entityType;
    // If any of this hits, it's a good candidate, but not 100% sure. Allow overwrite by a later subtype.
    } else if (!this.mainType && (isFirst || isNameMain || hasDelete)) {
      this.mainType = entityType;
    }
  },

  instanceOf : function(value) {
    return value instanceof AggregateEntityInstance;
  },

  newInstance : function(id) {
    return new AggregateEntityInstance(this, id);
  },

  /*
  instanceToJSON : function(instance) {
    assert(this.instanceOf(instance));
    // TODO set ID, but on the FooID field, not the Summary field
    return instance.id;
  },*/
}
extend(AggregateEntityType, EntityType);

AggregateEntityType.newType = function(fullName) {
  var type = gApp._allTypes[fullName];
  if (type) {
    return type;
  }
  type = new AggregateEntityType(fullName);
  gApp._addNewType(type);
  return type;
}


/**
 * A FooSummary type.
 * E.g. used for search and ...
 */
function SummaryType(fullName) {
  EntityType.call(this, fullName);
}
SummaryType.prototype = {
  /**
   * A FooRequest entity, which has this summary as <Return><EntityCollection> type.
   * {EntityType}
   */
  requestType : null,

  /**
   * @param jsonValue {JSON} E.g.
   * {
   *   Id: 2,
   *   Description: "Test record",
   *   ...
   * }
   */
  instanceFromJSON : function(json) {
    // id will be set by _parseJSONInstance()
    var result = this.newInstance(0);
    var errorCallback = (ex) => ddebug("Error while fetching Summary type " + this.fullName + ": " + ex); // TODO 
    this.fetchType(() => {
      this.fetchPropertyTypes(() => {
        result._parseJSONInstance(json);
      }, errorCallback);
    }, errorCallback);
    return result;
  },
}
extend(SummaryType, EntityType);

SummaryType.newType = function(fullName) {
  var type = gApp._allTypes[fullName];
  if (type) {
    return type;
  }
  type = new SummaryType(fullName);
  gApp._addNewType(type);
  return type;
}



function fetchTypeFromServer(fullName, resultCallback, errorCallback) {
  var fetch = new FetchHTTP({ url : gApp.baseURL + "/model/"  + sanitize.identifier(fullName) }, xmlDOM => {
    var namespaceRequested = getNamespace(fullName);
    var nsE = xmlDOM.querySelector("Namespace");
    var namespace = sanitize.identifier(nsE.getAttribute("Server"));
    namespace = namespace.replace("Rin.", ""); // @see makeFullName()
    assert(namespace == namespaceRequested, "Requested " + fullName + ", but got namespace " + namespace);
    var waiting = 1;
    var waiter = function() {
      if (--waiting == 0) {
        var result = gApp._allTypes[fullName];
        assert(result, fullName + " was not found in model XML file");
        resultCallback(result);
      }
    };
    querySelectorAll(nsE, "Aggregate").forEach(aggregateE => {
      var aggregateType = AggregateEntityType.newType(
          namespace + "." + sanitize.identifier(aggregateE.getAttribute("Name")));
      aggregateType._loaded = 1;
      querySelectorAll(aggregateE, "> Entity").forEach(entityE => {
        waiting++;
        var subType = parseEntityXML(entityE, namespace, 1, waiter, errorCallback);
        aggregateType._addSubType(subType);
      });
    });
    // {Map "FooSummary" typeName {String} -> FooSummariesRequest {EntityType}}
    var requestsForSummaries = {};
    querySelectorAll(nsE, "> Entity").forEach(entityE => {
      waiting++;
      var entityType = parseEntityXML(entityE, namespace, 1, waiter, errorCallback);
      var returnE = entityE.querySelector("Return > EntityCollection"); 
      if (returnE) { // this is a FooRequest type
        var summaryName = makeFullName(sanitize.identifier(returnE.getAttribute("Type").trim()), namespace);
        requestsForSummaries[summaryName] = entityType;
      } else if (requestsForSummaries[entityType.fullName]) {
        entityType.requestType = requestsForSummaries[entityType.fullName];
      }
    });
    querySelectorAll(nsE, "> Enum").forEach(enumE => {
      parseEnumDefintionXML(enumE, namespace);
    });
    waiter();
  }, errorCallback);
  fetch.start();
}

/**
 * @param entityE {DOMElement} <Entity> XML node. The Entity definition (not a reference/use).
 * @param namespace {String}
 * @param waitForPropertyEntitiesDepth {Integer} while fetching the model for the type
 *     of properties of type <Entity>:
 *     0 = do not wait
 *     1 = wait for depth level 1 only
 *     2 and higher (not implemented) = wait for depth n,
 *          e.g. entities ref from entities ref from entities etc..
 * @returns {EntityType}
 */
function parseEntityXML(entityE, namespace, waitForPropertyEntitiesDepth, resultCallback, errorCallback) {
  var typeName = sanitize.identifier(entityE.getAttribute("Name"));
  var fullName = namespace + "." + typeName;
  var entityType = strEndsWith(fullName, "Summary") ? SummaryType.newType(fullName) : EntityType.newType(fullName);
  entityType._loaded = 1;
  var descrE = entityE.querySelector("Description");
  var keyE = descrE.querySelector("Key");
  if (keyE && keyE.getAttribute("Name")) {
    entityType.keyPropertyName = sanitize.identifier(keyE.getAttribute("Name"));
    if (keyE.getAttribute("Alias")) {
      entityType.keyPropertyAlias = sanitize.identifier(keyE.getAttribute("Alias"));
    }
  }
  // TODO preserve order in model XML file
  var properties = [];
  var waiting = 0;
  // Preserve order of properties
  var properties = nodeListToArray(descrE.childNodes).map(pE => {
    var tag = pE.nodeName;
    if (tag == "Boolean") {
      return parsePropertyXMLTag(pE, booleanType);
    } else if (tag == "Decimal") {
      return parsePropertyXMLTag(pE, decimalType);
    } else if (tag == "String") {
      return parsePropertyXMLTag(pE, stringType);
    } else if (tag == "Date") {
      return parsePropertyXMLTag(pE, dateType);
    } else if (tag == "DateTime") {
      return parsePropertyXMLTag(pE, dateTimeType);
    } else if (tag == "Entity" || tag == "Enum") {
      var fullName = makeFullName(sanitize.identifier(pE.getAttribute("Type").trim()), namespace);
      var type = tag == "Entity"
          ? SummaryType.newType(fullName)
          : EnumType.newType(fullName);
      // Caller needs to call fetchPropertyTypes() later to fill it
      return parsePropertyXMLTag(pE, type);
    } else if (tag == "Int") {
      var name = sanitize.identifier(pE.getAttribute("Name"));
      // Dropdowns
      if (strEndsWith(name, "Id")) { // this is most likely an entity disguising as Integer
        var typeName = strRemoveEnd(name, "Id") + "Summary";
        // Could be in another namespace, so we need to search our whole list of types.
        // Also match LocationStatusSummary for StatusId (property name shorter than summary name)
        // TODO Also match BranchSummary for OriginatingBranchId (property name longer than summary name)
        //      For that, split words (ui.js), search for each word, and prefer the last word.
        var fullNames = gApp.getTypesList().filter(fn => strEndsWith(fn, typeName));
        if (fullNames.length > 1) { // several matches on *FooSummary, so look for exact FooSummary
          fullNames = fullNames.filter(fn => strEndsWith(fn, "." + typeName));
        }
        if (fullNames.length == 1) {
          var type = SummaryType.newType(fullNames[0])
          // Caller needs to call fetchPropertyTypes() later to fill it
          var propInfo = parsePropertyXMLTag(pE, type);
          if (propInfo) {
            propInfo.isInteger = true;
          }
          return propInfo;
        }
        return parsePropertyXMLTag(pE, integerType);
      } else { // plain normal integer
        return parsePropertyXMLTag(pE, integerType);
      }
    }
  });
  properties.forEach(propInfo => {
    if (!propInfo) {
      return;
    }
    entityType._propertiesMap[propInfo.name] = propInfo;
  });

  // Parse "Generate*" verbs
  nodeListToArray(entityE.childNodes).forEach(nodeE => {
    var tag = nodeE.nodeName;
    if (!strStartsWith(tag, "Generate")) return;
    tag = tag.substr(8);
    if (tag == "CreateRequestUpdateDelete" || tag == "BatchCreateRequestUpdateDelete" ||
        tag == "Crud" || tag == "BatchCrud") {
      entityType._verbs.push("Create");
      entityType._verbs.push("Request");
      entityType._verbs.push("Update");
      entityType._verbs.push("Delete");
    } else { // (tag == "Create" || tag == "Request" || tag == "Update" || tag == "Delete" || tag == "RequestAll") {
      entityType._verbs.push(sanitize.identifier(tag));
    }
  });
  if (arrayContains(entityType._verbs, "RequestAll")) {
    entityType.is1toN = true;
  }


  if (waiting == 0) {
    resultCallback(entityType);
  }
  return entityType;
}

/**
 * @param propertyE {DOMElement} <Int>, <String> etc. tags
 * @param type {JasminType} integerType, ..., EntityType
 * @param namespace {String} Only for EntityType
 * @returns {PropertyInfo} see EntityType.properties.
 * May be null, caller must filter
 */
function parsePropertyXMLTag(propertyE, type, namespace) {
  if (propertyE.getAttribute("Location") == "Server") {
    return null;
  }

  var propInfo = {
    type : type,
    name : sanitize.identifier(propertyE.getAttribute("Name").trim()),
    required : propertyE.getAttribute("Required") == "true",
    readonly : propertyE.hasAttribute("Access") && propertyE.getAttribute("Access") == "Entity",
  };

  nodeListToArray(propertyE.attributes).forEach(attr => {
    if (attr.name == "Type" || attr.name == "Name" || attr.name == "Required") {
      return; // already parsed, added as lowercase
    }
    var attrName = capitalize(sanitize.identifier(attr.name));
    var attrValue = sanitize.label(attr.value.trim());
    propInfo[attrName] = attrValue;
  });

  return propInfo;
}

/**
 * @param enumE {DOMElement} <Enum><Value .../><Enum> Enum definition (not a reference/use)
 * @returns {EnumType}
 */
function parseEnumDefintionXML(enumE, namespace) {
  var fullName = namespace + "." + sanitize.identifier(enumE.getAttribute("Name"));
  var enumType = EnumType.newType(fullName);
  enumType._loaded = 1;
  var i = 0;
  querySelectorAll(enumE, "Value").forEach(valueE => {
    var name = sanitize.identifier(valueE.getAttribute("Name"));
    enumType.options[name] = {
      name : name,
      description : sanitize.label(valueE.getAttribute("Description")),
      value : valueE.hasAttribute("Value") ? sanitize.integer(valueE.getAttribute("Value")) : i++,
    };
  });
  return enumType;
}

/**
 * @param fullName {String}
 * @returns namespace {String}
 */
function getNamespace(fullName) {
  var nameParts = fullName.split(".");
  var typeName = nameParts.pop();
  return nameParts.join(".");
}

/**
 * Given a namespace and a name identifier,
 * construct the full name.
 * The given name may be just the typeName without namespace,
 * with namespace but without Rin,
 * or the full name.
 *
 * Currently, we let the fullName not include "Rin." for brevity and readability.
 * See also CodeGen.Namespace parsing.
 *
 * @returns fullName {String}
 */
function makeFullName(name, namespace) {
  if (strStartsWith(name, "Rin.")) {
    //return name;
    return name.replace("Rin.", "");
  }
  if (!strContains(name, ".")) {
    return namespace + "." + name;
  }
  if (gApp._allTypes[name]) {
    return name;
  }

  // Complete the namespace from the left, until we find the name
  var namespaceParts = namespace.split(".");
  var fullerName = name;
  namespaceParts.forEach(part => {
    fullerName = part + "." + fullerName;
    if (gApp._allTypes[fullerName]) {
      return fullerName;
    }
  });

  // fallback: Use the plain name
  return name;
}



/*
Example requests and responses that we'll send and receive:
e.g. http://localhost:53470/api/json/syncreply/PropertyLocationMainRequest?id=35616

Response for <http://localhost:53470/api/json/syncreply/PropertyLocationLegalRequest?id=35616>:
{
	Id: 35616,
	IsTaxable: False,
	OwnershipTypeId: 1,
	LegalPossessionDocumentTypeSummary:
	{
		Id: 9,
		Number: 7,
		Description: Unknown,
		LocalDescription: Unknown,
		IsActive: True
	},
	LegalPossessionDocumentTypeId: 9,
	UserName: FFLINTSTONE,
	UserDateTime: "1014-04-27T08:32:49.4070000+02:00",
	ConcurrencyTokens:
	[
		"PtEWm3VfdYnJuJg2T4P9FTx/dTttJmT+/xjUSOb0JpI=: 35616:AAAAAt9MYkU="
	]
}

Sending response for <http://localhost:53470/api/json/syncreply/LeaseMainRequest?id=309>
{
	Id: 309,
	Number: 303,
	WhqNumber: ZAF1051-L01,
	Party: Lessee,
	PropertyType: Location,
	LocationSummary:
	{
		Id: 20774,
		Number: 1136,
		WhqNumber: ZAF1051,
		Description: Wattville,
		TransliteratedDescription: Wattville,
    ...
		FormattedAddress: "2453 Serema Street
WATTVILLE (GP-ZAF)
Gauteng
South Africa
"
	},
	LocationId: 20774,
	StartDate: 2016-11-03,
	EndDate: 2016-12-01,
	IsRenewable: False,
	IsImported: False,
	IsSentToWhq: False,
	UserName: fflintstone,
	UserDateTime: "1016-11-18T16:59:44.2070000+01:00",
	ConcurrencyTokens:
	[
		"8j9N67kDV4QgzRyx4FqOFgQptMElo54htOel2yMQjl0=: 309:AAAAAy4GRz4="
	]
}

The above shows JSV, because it's easier to read, but we're actually using JSON on the wire.
*/

function EntityInstance(entityType, id) {
  assert(entityType instanceof EntityType);
  assert(typeof(id) == "number");
  this.entityType = entityType;
  this.id = id;
  this.propertyValues = {};
  this._serverValues = {};
  this._previousValues = null; // must be null at fetch, will be filled in fireChangeObservers()
  this.changeObservers = [];
}
EntityInstance.prototype = {
  /**
   * {EntityType}
   */
  entityType : null,

  /**
   * Primary key for this object
   * {Int}
   */
  id : null,

  /**
   * {Map property name {String} -> property value for this object}
   */
  propertyValues : null,

  /**
   * The object changed, compared to the version from the server.
   * {Boolean}
   */
  _isDirty : false,

  /**
   * Same as propertyValues, when the change listener fired the last time
   * {Map property name {String} -> property value for this object}
   */
  _previousValues : null,

  /**
   * Same as propertyValues, when we fetched the data from the server
   * {Map property name {String} -> property value for this object}
   */
  _serverValues : null,

  /**
   * Allows to send data changes back to the server.
   * Server requires it to avoid data conflicts and accidental overwriting
   * of concurrent edits.
   * {Array of String}
   */
  _concurrencyTokens : null,

  /**
   * Called when the data changed
   * reasons: "user", "server" or "system"
   * {Array of {Function(reason)}
   */
  changeObservers : null,

  /**
   * Gets the current data record from the server
   * @returns {Abortable}
   */
  fetch : function(successCallback, errorCallback) {
    assert(typeof(this.entityType.keyPropertyName) == "string");
    assert(this.id, "Need ID to fetch entity instance");
    var args = {};
    args[this.entityType.keyPropertyName] = this.id;
    var fetch = new FetchHTTP({
      url : gApp.baseURL + "/api/json/syncreply/"  + sanitize.identifier(this.entityType.typeName) + "Request",
      urlArgs : args,
      uploadBody : args,
    }, json => {
      this._parseJSONInstance(json);
      successCallback();
      this.fireChangeObservers("server");
    }, errorCallback);
    fetch.start();
    return fetch;
  },

  _parseJSONInstance : function(json) {
    this.propertyValues = {};
    this.entityType.properties.forEach(propInfo => {
      var name = propInfo.name;
      var value = json[name];
      //assert(propInfo.type instanceof JasminType);
      if (propInfo.isInteger) { // Dropdown: Entity as Integer ID, not as Summary
        gApp.fetchReferences(propInfo.type, entityInstances => {
          this.propertyValues[name] = entityInstances.filter(instance => instance.id == value)[0];
        }, errorNonCritical);
      } else if (value !== undefined) { // This is the normal case
        this.propertyValues[name] = propInfo.type.instanceFromJSON(value);
      } else if (propInfo.type == booleanType) { // bool and no value set - can happen even for Required=true
        this.propertyValues[name] = false;
      } else if (propInfo.required) {
        // Server actually violates this in GET operations :-(
        // The server is probably thinking "I'm the boss here, the rules are for the others" :-P
        ddebug("Required property " + name + " is missing while parsing JSON for " +
            this.entityType.typeName + ": " + JSON.stringify(json, null, " "));
      } // else optional and not passed: Don't set it.
    });
    this.id = this.propertyValues[this.entityType.keyPropertyName] =
        sanitize.integer(json[this.entityType.keyPropertyName]);
    this._concurrencyTokens = json.ConcurrencyTokens;

    // Copy propertyValues to _serverValues, for undo
    this._serverValues = {};
    for (var propName in this.propertyValues) {
      this._serverValues[propName] = this.propertyValues[propName];
    }
    this._previousValues = null;
    this._isDirty = false;
  },

  fireChangeObservers : function(reason) {
    // For loop protection, check whether there are actual changes.
    var didChange = false;
    if (this._previousValues) {
      for (var propName in this.propertyValues) {
        if (this._previousValues[propName] != this.propertyValues[propName]) {
          didChange = true;
          this._isDirty = true;
        }
      }
    } else {
      didChange = true;
    }
    this._previousValues = {};
    for (var propName in this.propertyValues) {
      this._previousValues[propName] = this.propertyValues[propName];
    }
    if (!didChange) {
      return;
    }

    this.changeObservers.forEach(func => {
      try {
        func(reason);
      } catch (ex) {
        errorNonCritical(ex);
      }
    });
  },

  /**
   * The object changed, compared to the version from the server.
   * @returns {Boolean}
   */
  isDirty : function() {
    return this._isDirty;
  },

  /**
   * Resets this object to the values last fetched from the server.
   */
  undo : function() {
    //if (!this._isDirty) return;
    for (var propName in this.propertyValues) {
      this.propertyValues[propName] = this._serverValues[propName];
    }
    this._previousValues = null;
    this._isDirty = false;
    this.fireChangeObservers("user");
  },

  /**
   * Sends the local changes to the server for processing and storage.
   *
   * @returns {Abortable}
   */
  save : function(successCallback, errorCallback) {
    assert(typeof(this.entityType.keyPropertyName) == "string");
    assert(this.id, "Need ID to fetch entity instance");
    assert(!(this.entityType instanceof SummaryType), "Cannot save a summary");
    var isCreate = !this._concurrencyTokens;

    var fetch = new FetchHTTP({
      url : gApp.baseURL + "/api/json/syncreply/" +
          sanitize.identifier(this.entityType.typeName) +
          (isCreate ? "CreateRequest" : "UpdateRequest"),
      method : "POST",
      uploadBody : this.savePart(isCreate),
    }, jsonResult => {
      this._concurrencyTokens = jsonResult.ConcurrencyTokens;
      var serverId = sanitize.integer(jsonResult.Changes[0].ServerId);
      if (isCreate) {
        this.id = serverId;
        this.fireChangeObservers("server");
      } else {
        assert(this.id == serverId, "Returned server ID does not match what we sent in the update");
      }
      successCallback();
    }, ex => {
      errorCallback(convertJasminException(ex));
    });
    fetch.start();
    return fetch;
  },

  /**
   * Creates the JSON to send the changes of this part to the server.
   * This allows the Aggregate to send all updates together in on request.
   *
   * @param isCreate{Boolean}
   *    true, if creating a new entity that didn't exist before.
   *    false, if this is an update.
   * @returns {JSON}
   */
  savePart : function(isCreate) {
    assert(!(this.entityType instanceof SummaryType), "Cannot save a summary");
    assert(typeof(this.entityType.keyPropertyName) == "string");
    assert((isCreate && !this.id) || (!isCreate && this.id),
        isCreate ? "Cannot save an existing entity that already has an ID" : "Need ID to update");

    var json = {};
    if (!isCreate) {
      json.ConcurrencyTokens = this._concurrencyTokens;
      json[this.entityType.keyPropertyName] = this.id;
    }
    this.entityType.properties.forEach(propInfo => {
      if (propInfo.readonly) return;
      var value = this.propertyValues[propInfo.name];
      if (!value) {
        if (propInfo.type.name == "boolean") {
          // default false. Do send it.
        } else {
          assert(!propInfo.required, propInfo.name + " is required");
          return; // Do not send empty values
        }
      }

      json[propInfo.name] = propInfo.type.instanceToJSON(value);
    });
    return json;
  },

  /**
   * Gets a short string that represents this object to the end user.
   * Usually the description, or ID, or a combination.
   */
  title : function() {
    var tryProperties = [ "Name", "LocalDescription", "Description", "Title", "Number" ];
    for (var i = 0; i < tryProperties.length; i++) {
      var propName = tryProperties[i];
      if (this.propertyValues[propName]) {
        return sanitize.label(this.propertyValues[propName]);
      }
    }
    return "";
  },
}

function AggregateEntityInstance(entityType, id) {
  EntityInstance.call(this, entityType, id);
  assert(entityType instanceof AggregateEntityType);
  this._subInstances = entityType.subTypes.map(subType => {
    var i = subType.newInstance(id);
    if (subType == this.entityType.mainType) {
      this.mainInstance = i;
    }
    return i;
  });
  this._subListInstances = {};
  entityType.subListTypes.forEach(subType => {
    this._subListInstances[subType.fullName] = [];
  });

  // Forward changes in subInstances to observers on this aggregate.
  var changeObserver = reason => {
    this.fireChangeObservers(reason);
  };
  this._subInstances.forEach(i => i.changeObservers.push(changeObserver));
  // TODO subListInstances, too?
}
AggregateEntityInstance.prototype = {
  /**
   * The "Main" instance.
   * {EntityInstance}
   */
  mainInstance : null,

  /**
   * @See subInstances()
   * {Array of {EntityInstance}}
   */
  _subInstances : null,

  /**
   * All sub instances that are in 1:n lists.
   * @see subListInstances()
   * {Map of subtype typeName {String} -> Array of {EntityInstance}}}
   */
  _subListInstances : null,

  /**
   * All 1:1 sub entity instances.
   * Includes Main. Does not include subListInstances.
   * {Array of {EntityInstance}}
   */
  get subInstances() {
    return this._subInstances;
  },

  /**
   * The 1:n collection of sub instances of |type|.
   * @param type {EntityType} get all instances of this sub entity type
   * @returns {Array of {EntityInstance}}
   *    // TODO should return observable collection instead.
   */
  getSubListInstances : function(type) {
    assert(type instanceof EntityType);
    return this._subListInstances[type.typeName];
  },

  /**
   * Loads the content/instances of all subTypes.
   *
   * Only direct 1:1 sub groups. 1:n instances are fetches using fetchList().
   */
  fetch : function(successCallback, errorCallback) {
    var waiting = 0;
    this.subInstances.forEach(subInstance => {
      subInstance.fetch(() => {
        if (--waiting == 0) {
          this.fireChangeObservers("server");
          successCallback();
        }
      }, errorCallback);
    });
    if (!waiting) {
      successCallback();
    }
    // TODO return Abortable
  },

  /**
   * Fetches all records for this the given detail table.
   *
   * @param subListType {EntityType} One of the entries of subListTypes.
   * @param resultCallback {Function(Array of {EntityInstance})}
   *     All detail items of type subListType
   * @returns {Abortable}
   */
  fetchList : function(subListType, resultCallback, errorCallback) {
    assert(subListType instanceof EntityType);
    assert(arrayContains(this.entityType.subListTypes, subListType));

    // find the ID
    var idProps = subListType.properties.filter(propInfo => {
      return propInfo.AssociatedParent;
    });
    assert(idProps.length, "No AssociatedParent found for " + subListType.typeName + " in " + this.entityType.fullName);
    assert(idProps.length == 1, "Multiple AssociatedParents found for " + subListType.typeName + " in " + this.entityType.fullName);
    var parentProp = idProps[0];
    /*var parentType = this.mainType; // We only guessed this. Check our assumptions:
    assert(idProp.AssociatedParent == parentType.typeName, "AssociatedParent is " + parentProp[0].AssociatedParent +
        ", but we thought it would be " +parentType.typeName);*/
    var parentType = this.entityType.getSubType(sanitize.identifier(parentProp.AssociatedParent));
    assert(parentType, "Parent type " + parentProp.AssociatedParent + " not found in " + this.entityType.fullName);
    assert(parentType.keyPropertyAlias == parentProp.name || parentType.getProperty(parentProp.name), "ID property " + parentProp.name + " not found in " + parentType.fullName);

    var parentInstance = this.subInstances.filter(i => i.entityType == parentType)[0];
    assert(parentInstance, "Parent instance " + parentType.typeName + " not found in " + this.entityType.fullName);
    //assert(mapToArray(parentInstance.propertyValues).length, "Parent instance " + parentType.typeName + " not yet loaded in " + this.entityType.fullName);
    var parentID;
    if (parentType.keyPropertyAlias == parentProp.name) {
      parentID = parentInstance.id;
    } else {
      parentID = parentInstance.propertyValues[parentProp.name];
    }
    assert(parentID, "Parent instance property " + parentProp.name + " not populated in " + this.entityType.fullName);

    var args = {};
    args[parentProp.name] = parentID;
    var fetch = new FetchHTTP({
      url : gApp.baseURL + "/api/json/syncreply/"  + sanitize.identifier(subListType.typeName) + "sRequest",
      urlArgs : args,
      uploadBody : args,
    }, json => {
      assert(Array.isArray(json), "Expected a list of items");
      var results = json.map(jsonInstance => {
        var item = subListType.newInstance(0);
        item._parseJSONInstance(jsonInstance);
        return item;
      });
      this._subListInstances[subListType.typeName] = results;
      resultCallback(results);
      this.fireChangeObservers("server"); // TODO use observable collection instead
    }, errorCallback);
    fetch.start();
    return fetch;
  },

  fireChangeObservers : function(reason) {
    this.changeObservers.forEach(func => {
      try {
        func(reason);
      } catch (ex) {
        errorNonCritical(ex);
      }
    });
  },

  isDirty : function() {
    if (this.subInstances.some(i => i.isDirty()) ||
        mapToArray(this._subListInstances).some(list => list.some(i => i.isDirty()))) {
      return true;
    }
    return false;
  },

  undo : function() {
    this.subInstances.forEach(subInstance => subInstance.undo());
    mapToArray(this._subListInstances).forEach(list => list.forEach(subInstance => subInstance.undo())); // needed?
  },

  /* This is an example aggregate update request
  Request to <http://localhost:53470/api/json/syncreply/PropertyLocationUpdateRequest>
  {
    PropertyLocationMainUpdateRequest:
    {
      Id: 21928,
      Description: Test of city associated,
      TransliteratedDescription: Test of city associated,
      Notes: ophj,
      GeoLocationId: 209,
      PrimaryUseId: 3,
      StatusId: 1,
      NeighborhoodTypeId: 3,
      StatusDateTime: "2015-09-07T00:00:00.0000000+02:00",
      ConcurrencyTokens:
      [
        "PtEWm3VfdYnJuJg2T4P9FTx/dTttJmT+/xjUSOb0JpI=: 21928:AAAAAzuKk1c="
      ]
    },
    PropertyLocationLegalUpdateRequest:
    {
      Id: 21928,
      IsTaxable: False,
      OwnershipTypeId: 4,
      LegalPossessionDocumentTypeId: 3,
      ConcurrencyTokens:
      [
        "PtEWm3VfdYnJuJg2T4P9FTx/dTttJmT+/xjUSOb0JpI=: 21928:AAAAAzuKk1c="
      ]
    }
  }

  This is the response:
  {
    ConcurrencyTokens:
    [
      "PtEWm3VfdYnJuJg2T4P9FTx/dTttJmT+/xjUSOb0JpI=: 21928:AAAAAzuKk1s="
    ],
    Changes:
    [
      {
        SourceIdentifiers:
        [
          PtEWm3VfdYnJuJg2T4P9FTx/dTttJmT+/xjUSOb0JpI=
        ],
        ClientId: 21928,
        ServerId: 21928,
        UserName: fflintstone,
        UserDateTime: "2016-12-02T07:13:38.5470000+01:00"
      }
    ]
  }
  */

  /**
   * Sends the local changes of this whole entity
   * to the server for processing and storage.
   *
   * TODO subListTypes
   *
   * @returns {Abortable}
   */
  save : function(successCallback, errorCallback) {
    assert(this.id, "Need ID to fetch entity instance");
    var isCreate = !this.id;

    var fetch = new FetchHTTP({
      url : gApp.baseURL + "/api/json/syncreply/" +
          sanitize.identifier(this.entityType.typeName) +
          (isCreate ? "CreateRequest" : "UpdateRequest"),
      method : "POST",
      uploadBody : this.savePart(isCreate),
    }, jsonResult => {
      this._concurrencyTokens = jsonResult.ConcurrencyTokens;
      var serverId = sanitize.integer(jsonResult.Changes[0].ServerId);
      this.subInstances.forEach(subInstance => {
        subInstance._concurrencyTokens = jsonResult.ConcurrencyTokens;
        if (isCreate) {
          subInstance.id = serverId;
        } else {
          assert(subInstance.id == serverId, "Returned server ID does not match what we sent in the update for the subInstance " +
              subInstance.entityType.typeName);
        }
      });
      if (isCreate) {
        this.id = serverId;
        this.fireChangeObservers("server");
      } else {
        assert(this.id == serverId, "Returned server ID does not match what we sent in the update");
      }
      successCallback();
    }, ex => {
      errorCallback(convertJasminException(ex));
    });
    fetch.start();
    return fetch;
  },

  savePart : function(isCreate) {
    var json = {};
    this.subInstances.forEach(subInstance => {
      if (!subInstance.isDirty) return;
      json[sanitize.identifier(subInstance.entityType.typeName) +
           (isCreate ? "CreateRequest" : "UpdateRequest")]
          = subInstance.savePart();
    });
    return json;
  },

  title : function() {
    return this.mainInstance.title();
  },
}
extend(AggregateEntityInstance, EntityInstance);


/**
 * An error coming back from the server.
 *
 * @param ex {ServerException} The exception coming from FetchHTTP
 *     after calling the server ServiceStack functions.
 */
function JasminException(ex) {
  assert(ex instanceof ServerException);
  assert(ex.rootErrorMsg);
  ServerException.call(this, ex.rootErrorMsg, ex.code, ex.uri);
  this.stack = ex.stack;
  this.resultBody = ex.resultBody;
  // rootErrorMsg = e.g. "OperationFailedException"
  this._message = this.getErrorMessage(ex.rootErrorMsg);
  this._errors = [];

  if (this.resultBody && typeof(this.resultBody) == "object") {
    /* result =
    {
      Changes: [],
      ConcurrencyTokes: [],
      ResponseStatus:
      (JSON in string) {
        ConcurrencyError: ":0:",
        CorrelationId: "546672b58e1c4bf4a7d7aa8c1e65f531",
        EntityId: 0,
        ErrorCode: "ValidationError",
        Errors: [
          (JSON in string) {
            ErrorCode: "Core_Validation_Unspecified",
            FieldName: "CurrencyId",
            Key: 56,
            FormattedMessage: {
                ResourceKey: "CurrencyId is required.",
                MessageParameters: []
            }
          }
        ]
      }
    }
    */
    var result = this.resultBody;
    ddebug(JSON.stringify(result, null, " "));
    if (result.ResponseStatus) {
      var responseStatus = JSON.parse(result.ResponseStatus);
      ddebug(JSON.stringify(responseStatus, null, " "));
      this.message = this.getErrorMessage(responseStatus.ErrorCode);
      if (Array.isArray(responseStatus.Errors)) {
        this._errors = responseStatus.Errors.map(errorJSONStr => {
          var errorJSON = JSON.parse(errorJSONStr);
          ddebug(JSON.stringify(errorJSON, null, " "));
          var error = {};
          error.message = this.getErrorMessage(errorJSON.ErrorCode);
          error.fieldName = sanitize.identifier(errorJSON.FieldName);
          error.id = sanitize.integer(errorJSON.Key);
          if (errorJSON.FormattedMessage &&
              errorJSON.FormattedMessage.ResourceKey &&
              strContains(errorJSON.FormattedMessage.ResourceKey, " ")) {
            error.message = sanitize.label(errorJSON.FormattedMessage.ResourceKey);
          }
          return error;
        });
      }
    }
  }
}
JasminException.prototype = {
  _errors : null,
  isValidationError : false,

  /**
   * @returns {Array of {
   *   type {String-Enum}, either "validation" or "global"
   *   message {String}, message to show to end user
   *   fieldName {String}, (Optiona) property name on an
   *       EntityType/EntityInstance, where the error happened
   *   id {Integer}, EntityInstance.id for the object where the error happened.
   * }}
   */
  get errors() {
    return this._errors;
  },

  /**
   * @param serverErrorCode {String}
   * @returns {String} a message to show to the end user
   */
  getErrorMessage : function(serverErrorCode) {
    var label = typeNameReadable(
        strRemoveEnd(sanitize.identifier(serverErrorCode), "Exception"));
    // TODO look up resource
    return label;
  },

  /**
   * @param serverErrorCode {String}
   * @returns {String} a message to show to the end user
   */
  getErrorMessageWithParameters : function(serverErrorCode, params) {
    // TODO
    return this.getErrorMessage(serverErrorCode);
  },
}
extend(JasminException, ServerException);

function convertJasminException(ex) {
  if (ex instanceof ServerException) {
    return new JasminException(ex);
  }
  return ex;
}


/* Search
Example request and response:
Simple search:
Request to <http://localhost:53470/api/json/syncreply/PropertyImprovementSummariesRequest?take=201&searchString=test>
{
	Take: 201,
	SearchString: test
}

Extended search:
Request to <http://localhost:53470/api/json/syncreply/PropertyImprovementSummariesRequest?number=5&description=descr&take=201>
{
	Number: 5,
	Description: descr,
	Take: 201
}

Response:
[
	{
		Id: 34,
		Number: 7,
		WhqNumber: ZAF1228-I01,
		Description: Parking lot,
		IsImported: True,
		LocationDescription: Test of city changes associatied,
		LocationId: 21928
	},
	{
		Id: 35,
		Number: 8,
		WhqNumber: ZAF1228-I02,
		Description: Garden,
		IsImported: True,
		LocationDescription: Test of city changes associatied,
		LocationId: 21928
	}
]
*/

/**
 * @param resultType {SummaryType} The Summary type that will be the result of the request.
 *    this.entityType will be resultType.requestType {EntityType}
 *    and will determine the search parameters.
 *    The summaryType will determine the search results.
 *    Attention: this.entityType != resultType.
 */
function SearchRequest(resultType) {
  assert(resultType instanceof SummaryType);
  assert(resultType.requestType && resultType.requestType instanceof EntityType);
  EntityInstance.call(this, resultType.requestType, 0);
  this.requestType = resultType.requestType;
  assert(this.entityType == this.requestType);
  this.resultType = resultType;
  this.searchResults = [];
}
SearchRequest.prototype = {
  /**
   * {EntityType}
   */
  requestType : null,
  /**
   * {SummaryType}
   */
  resultType : null,

  searchResults : null,

  /**
   * Extended search mode.
   * Use the search parameters in the properties of this instance.
   *
   * @param maxResults {Integer} return no more than this many results
   * @param resultCallback {Function(searchResults)}
   *    searchResults {Array of summaryInstance {EntityInstance}}
   *    whereby summaryInstance.entityType == this.resultType
   * @returns {Abortable}
   */
  fetch : function(maxResults, resultCallback, errorCallback) {
    var args = {
      Take : maxResults || 30,
    };
    this.requestType.properties.forEach(propInfo => {
      var value = this.propertyValues[propInfo.name];
      if (value) {
        args[propInfo.name] = propInfo.type.instanceToJSON(value);
      }
    });
    var fetch = new FetchHTTP({
      url : gApp.baseURL + "/api/json/syncreply/"  + sanitize.identifier(this.requestType.typeName),
      urlArgs : args,
      uploadBody : args,
    }, json => {
      var results = this._parseJSONResults(json);
      resultCallback(results);
      this.fireChangeObservers("server");
    }, errorCallback);
    fetch.start();
    return fetch;
  },

  /**
   * Simple search mode
   * Just uses the search string passed here.
   * Ignores the property contents.
   *
   * @param simpleSearch {String} simple search string,
   *     as entered in the simple mode of the search forms.
   * @param maxResults {Integer} return no more than this many results
   * @param resultCallback @see fetch()
   * @returns {Abortable}
   */
  fetchSimple : function(simpleSearch, maxResults, resultCallback, errorCallback) {
    simpleSearch = sanitize.label(sanitize.nonemptystring(simpleSearch));
    var args = {
      SearchString : simpleSearch,
      Take : maxResults || 30,
    };
    var fetch = new FetchHTTP({
      url : gApp.baseURL + "/api/json/syncreply/"  + sanitize.identifier(this.requestType.typeName),
      urlArgs : args,
      uploadBody : args,
    }, json => {
      var results = this._parseJSONResults(json);
      resultCallback(results);
      this.fireChangeObservers("server");
    }, errorCallback);
    fetch.start();
    return fetch;
  },

  _parseJSONResults : function(json) {
    this.searchResults = json.map(jsonInstance => {
      var result = this.resultType.newInstance(0);
      result._parseJSONInstance(jsonInstance);
      return result;
    });
    return this.searchResults;
  },
}
extend(SearchRequest, EntityInstance);
