﻿/**
 * This is a singleton.
 * Stores and gives access to core data.
 */
function Application(loadCompleteCallback, errorCallback)
{
  this._allTypes = {};
  this._waitingForNamespaces = {};
  this.strings = new Resources();
  this._referencesDataCache = {};
  this.baseURL = "";

  this.loadTypesList(loadCompleteCallback, errorCallback);
}
Application.prototype = {
  /**
   * All entity types
   * TODO should this include subentities or just the aggregate?
   * {Map full name with name space {String} -> type {EntityType}}
   * May be null, if type is not yet loaded
   */
  _allTypes : null,

  /**
   * While a type has been requested and is being fetched from the server,
   * but not received yet, it gets an entry here and you can add a callback
   * to know when it arrived.
   * Used by EntityType.fetchType() only.
   * {Map namespace {String} -> Callbacks {Array of Function(JasminType)}}
   */
  _waitingForNamespaces : null,

  /**
   * {Resources}
   */
  strings : null,

  /**
   * Contains a cache of all dropdown entries for all types.
   * This is populated and read by EntityDropdownField,
   * but kept here, to keep the cache globally accessible.
   *
   * {Map type fullName {String} -> {Array of {EntityInstance}}}
   */
  _referencesDataCache : null,

  // util
  baseURL : null, // {String} URL of root of the current server

  loadTypesList : function(successCallback, errorCallback) {
    var fetch = new FetchHTTP({ url : this.baseURL + "/model/" }, plaintextList => {
      plaintextList.split("\n").forEach(fullName => {
        try {
          this._allTypes[sanitize.identifier(fullName)] = null;
        } catch (ex) {
          errorNonCritical(ex);
        }
      });
      successCallback();
    }, errorCallback);
    fetch.start();
  },

  /**
   * A list of all types known.
   * @returns {Array of String}
   */
  getTypesList : function() {
    var result = [];
    for (var name in this._allTypes) {
      result.push(name);
    };
    return result;
  },

  /**
   * A list of all *Summary types known.
   * @returns {Array of String}
   */
  getSummariesList : function() {
    var result = [];
    for (var name in this._allTypes) {
      if (strEndsWith(name, "Summary") && // and have entity, too
          this._allTypes[name.replace("Summary", "")] !== undefined) {
        result.push(name);
      }
    };
    return result;
  },

  /**
   * Returns all (!) summary entities of this type.
   * Goes to the server and fetches them all.
   * Caches them, so that they are fetched only once per app instance.
   * This is for dropdowns.
   * @param type {SummaryType} Fetch all (!) entities of this type
   * @returns {Array of EntityInstance}
   */
  fetchReferences : function(type, resultCallback, errorCallback) {
    assert(type instanceof SummaryType);
    var instances = this._referencesDataCache[type.fullName];
    if (instances) {
      resultCallback(instances);
      return;
    }
    type.fetchType(() => {
      type.fetchPropertyTypes(() => {

        var search = new SearchRequest(type);
        search.fetchSimple("*", 30, instances => {
          gApp._referencesDataCache[type.fullName] = instances;
          resultCallback(instances);
        }, errorCallback);
      }, errorCallback);
    }, errorCallback);
  },

  /**
   * Resolves a fullName into an EntityType.
   *
   * @param fullName {String} type name with namespace
   * @result {JasminType}
   */
  fetchInitialType : function(fullName, resultCallback, errorCallback) {
    assert(typeof(fullName) == "string");
    var type = this._allTypes[fullName];
    if (type && type._loaded == 2) {
      resultCallback(type);
      return;
    } else if (type) {
      type.fetchType(() => {
        type.fetchPropertyTypes(() => {
          resultCallback(type);
        }, errorCallback);
      }, errorCallback);
      return;
    }

    fetchTypeFromServer(fullName, type => {
      type.fetchPropertyTypes(() => {
        resultCallback(type);
      }, errorCallback);
    }, errorCallback);
  },

  /**
   * for model.js
   * @param type {JasminType}
   */
  _addNewType : function(type) {
    assert(type instanceof JasminType);
    assert(!(type instanceof PrimitiveType));
    assert(!this._allTypes[type.fullName], type.fullName + " is already known");
    this._allTypes[type.fullName] = type;
  }
}

/**
 * Gives access to localized label strings.
 *
 * Fetches and parses Resources.resx.
 */
function Resources() {
  this._resources = {};
}
Resources.prototype = {
  _resources : null, // {Map resource name -> value {String})

  appName : "Jasmin",

  /**
   * Gets a translated label.
   * Runs sync. Will not try to fetch. You need to make sure that you have called getAsync()
   * for this entity/module and waited for the result before calling get().
   *
   * @param resourceName {String} e.g. "Purchasing_Inventory_References_InventorySite_UserGroups_SectionName"
   * @returns translated label
   *     May be "", if not loaded yet.
   */
  get : function(resourceName) {
    var label = this._resources[resourceName];
    if (label) {
      return label;
    }
    return "";
  },

  /**
   * @param resourceName {String} e.g. "Purchasing_Inventory_References_InventorySite_UserGroups_SectionName"
   */
  getAsync : function(resourceName, resultCallback, errorCallback) {
    assert(resourceName && typeof(resourceName) == "string");
    var label = this._resources[resourceName];
    if (label) {
      resultCallback(label)
      return;
    }
    var parts = resourceName.split("_");
    var moduleName = parts[0];
    var fetch = new FetchHTTP({ url : gApp.baseURL + "/resources/" + moduleName }, xmlDOM => {
      // Resource.resx XML as DOM
      querySelectorAll(xmlDOM, "data").forEach(dataE => {
        var name = sanitize.alphanumdash(dataE.getAttribute("name")
            .replace("/", "-")); // Workaround for "Wind/hail" in Accounting
        var value = sanitize.label(dataE.firstElementChild.textContent);
        assert(name, "Error while parsing Resources.resx: name not found");
        // value == "" is allowed
        if (this._resources[name]) {
          return;
        }
        this._resources[name] = value;
      });
      var label = this._resources[resourceName];
      // Don't error on empty label. Callers usually just prefetch, so the label might not exist, but that shouldn't stop the page from loading.
      resultCallback(label);
    }, errorCallback);
    fetch.start();
  },

  /**
   * @param type {EntityType}
   * @returns label {String}
   */
  getLabelForType : function(type) {
    return this.get(this.getResourceNameForType(type) + "_DisplayName");
  },

  /**
   * @param type {EntityType}
   * @param resultCallback {Function(label {String})}
   */
  fetchLabelsForType : function(type, resultCallback, errorCallback) {
    this.getAsync(this.getResourceNameForType(type) + "_DisplayName", resultCallback, errorCallback);
  },

  /**
   * @param type {EntityType}
   * @param propertyName {String}
   * @returns label {String}
   */
  getLabelForProperty : function(type, propertyName) {
    var baseName = strReplaceAll(type.fullName, ".", "_");
    var result;
    if (type instanceof SummaryType) {
      baseName = strRemoveEnd(baseName, "Summary") + "Search";
    }

    result = this.get(baseName + "_" + propertyName);
    if (result) return result;

    propertyName = strRemoveEnd(propertyName, "Summary");
    propertyName = strRemoveEnd(propertyName, "Id");
    if (strStartsWith(propertyName, "Is")) {
      propertyName = propertyName.substr(2);
    }
    result = this.get(baseName + "_" + propertyName);
    return result;
  },

  /**
   * @param type {EntityType}
   * @returns resource name {String}
   */
  getResourceNameForType : function(type) {
    var baseName = strReplaceAll(type.fullName, ".", "_").replace("Summary", "Search");
    if (type.is1toN) {
      baseName += "s";
    }
    return baseName;
  },

  /**
   * Allows you to put a "*" in the search, which can stand for any number of characters.
   * @param resourceNameWithWildcard
   * @returns {Array of {
   *   resourceName {String},
   *   label {String},
   * }}
   */
  getWildcard : function(resourceNameWithWildcard) {
    var parts = resourceNameWithWildcard.split("*");
    var startsWith = parts.shift();
    var endsWith = parts.pop();
    var result = [];
    for (var resourceName in this._resources) {
      if ((!startsWith || strStartsWith(resourceName, startsWith)) &&
          (!endsWith || strEndsWith(resourceName, endsWith)) &&
          (!parts.length || parts.every(part => strContains(resourceName, part)))) {
        result.push({
          resourceName : resourceName,
          label : this._resources[resourceName],
        });
      }
    }
    return result;
  },
}
