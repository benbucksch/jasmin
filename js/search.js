/**
 * Displays a search page to search for entities of a given type.
 *
 * URL: #fullName=Facilities.PropertyManagement.Properties.Lease
 */

var gApp; // {Application} singleton -- will be set by page opener
var gSearchParams; // {EntityInstance with type instanceof SummaryType}  What the user searches for, esp. in extended search mode
var gIsExtendedSearch = false; // {Boolean} false = simple search mode; true = extended search mode
var gSelectedItems = []; // {Array of EntityInstance} Which summaries the user selected using the selector checkboxen. Do not change this directly, only by calling selectedItemsChanged(selectedItems).
var selectedItemsChanged = function() {}; // will be overwritten below. TODO replace this hack with observable collections
var gResults = [];
var gViewMode = 2; // 1 = grid, 2 = cards, 3 = map

function onLoad() {
  getAppContext();
  var query = parseURLQueryString(window.location.hash.replace("#", ""));
  var fullName = sanitize.identifier(query.fullname);
  assert(strEndsWith(fullName, "Summary", "Need Summary type to search"));
  setTitleFromFullName(fullName);
  hookupReturnKey(E("simpleSearchField"), startSimpleSearch);
  E("simpleSearchField").focus();

  // Get all the data
  gApp.fetchInitialType(fullName, summaryType => {
    assert(summaryType instanceof SummaryType);
    // prefetch all resources for this module
    gApp.strings.fetchLabelsForType(summaryType, () => {
      var searchParams = new SearchRequest(summaryType);
      gSearchParams = searchParams;
      setTitleFromEntity(summaryType);
      buildSearchPage(searchParams);
      createExtendedSummary(searchParams);

      if (query.simpleSearch) {
        E("simpleSearchField").value = sanitize.label(query.simpleSearch);
        startSimpleSearch();
      }
    }, errorCritical);
  }, errorCritical);
}
window.addEventListener("DOMContentLoaded", onLoad, false);

function buildSearchPage(searchParams) {
  var summaryType = searchParams.resultType;
  var resourceTypeName = gApp.strings.getResourceNameForType(summaryType);

  E("simpleSearchField").setAttribute("placeholder", "Search " + gApp.strings.getLabelForType(summaryType));
  E("simpleSearchInfo").title = gApp.strings.get(resourceTypeName + "Inline_InfoContent");

  // Getting the labels for the request fields (= extended search form fields) isn't trivial.
  // The client sorts the fields into tabs, and these tabs appear in the resource name.
  // The model doesn't have this separation at all, so we can't get to it.
  // Luckily, all the strings are in the same file, so they are all loaded,
  // so we'll make a wildcard search for them.
  // Little bonus is that this in turns lets us get to the client groups again *grin*,
  // so we can make the groups again. :) Yeah, we're sneaky. We'll get you!
  var resources = gApp.strings.getWildcard(resourceTypeName + "_*");

  // {Map group name {String} -> tab content pane {DOMElement}}
  var tabContentPaneEs = {};
  var tabs = new Tabs();
  var extendedSearchFieldsE = E("extendedSearchFields");
  extendedSearchFieldsE.appendChild(tabs.tabContentPanesE);
  extendedSearchFieldsE.appendChild(tabs.tabHeadersE);

  searchParams.entityType.properties.forEach(propInfo => {
    var field = newFieldForProperty(searchParams, propInfo);
    if (!field) return; // TODO remove once we support all types

    // Find the label
    var label;
    var resOptions = resources.filter(res => strEndsWith(res.resourceName, "_" + propInfo.name));
    var res = resOptions[0];
    if (resOptions.length > 1) { // when field is also a result field, there's also a resource name without tab name
      res = resOptions.sort((a, b) => b.resourceName.length - a.resourceName.length)[0]; // pick longest resource name, which hopefully is the one with the tab name
    }
    // Fallbacks, using Rin conventions
    if (!res && strStartsWith(propInfo.name, "Is")) { // IsFoo
      res = resources.filter(res => strEndsWith(res.resourceName, "_" + propInfo.name.substr(2)))[0];
    } else if (!res &&
        (strEndsWith(propInfo.name, "Summary") ||
         strEndsWith(propInfo.name, "Id"))) { // FooSummary or FooId
      var propNameBase = strRemoveEnd(strRemoveEnd(propInfo.name, "Summary"), "Id");
      res = resources.filter(res => strEndsWith(res.resourceName, "_" + propNameBase + "Id"))[0];
      if (!res) {
        res = resources.filter(res => strEndsWith(res.resourceName, "_" + propNameBase + "Summary"))[0];
      }
      if (!res) {
        res = resources.filter(res => strEndsWith(res.resourceName, "_" + propNameBase))[0];
      }
    } else if (!res && strEndsWith(propInfo.name, "Id")) { // FooId
      res = resources.filter(res => strEndsWith(res.resourceName, "_" + strRemoveEnd(propInfo.name, "Id")))[0];
      if (!res) {
        res = resources.filter(res => strEndsWith(res.resourceName, "_" + strRemoveEnd(propInfo.name, "Id") + "Summary"))[0];
      }
    } else if (!res && strEndsWith(propInfo.name, "From")) { // FooFrom
      res = resources.filter(res => strEndsWith(res.resourceName, "_" + strRemoveEnd(propInfo.name, "From")))[0];
      if (res) {
        label = res.label + " from"; // TODO l10n
      }
    } else if (!res && strEndsWith(propInfo.name, "To")) { // FooTo
      res = resources.filter(res => strEndsWith(res.resourceName, "_" + strRemoveEnd(propInfo.name, "To")))[0];
      if (res) {
        label = res.label + " to"; // TODO l10n
      }
    }
    if (res && !label) {
      label = res.label;
    }
    if (!label) {
      label = typeNameReadable(propInfo.name); // fallback, if we don't find a resource
    }
    propInfo.label = label;

    // Get the tab name and label
    var groupName;
    var groupLabel;
    if (res) {
      var resNameEnd = res.resourceName.substr(resourceTypeName.length + 1);
      var resParts = resNameEnd.split("_");
      if (resParts.length > 1) {
        groupName = resParts[0];
        groupLabel = gApp.strings.get(resourceTypeName + "_" + groupName) || groupName;
      }
    }
    if (!groupName) { // fallback
      groupName = "Misc";
      groupLabel = "Misc";
    }

    // Create tab
    var tabContentPane = tabContentPaneEs[groupName];
    if (!tabContentPane) {
      tabContentPane = tabContentPaneEs[groupName] = cE(null, "hwrap", "tabContent");
      tabs.addTab(groupLabel, tabContentPane);
    }
    // Add to tab
    tabContentPane.appendChild(new LabeledField(label, field).element);
  });

  addButtons(summaryType);
  showSearchHeader([]);
}

/**
 * Create the blue widgets in the search line
 */
function createExtendedSummary(searchParams) {
  /**
   * Criteria fields that we already created and that are visible.
   * {Map property name {String} -> criteria box {DOMElement}}
   */
  var fieldForProperty = {};
  var summaryLineE = E("extendedSearchSummary");
  /* Runs on every keypress, must be fast! Early exit quickly. */
  searchParams.changeObservers.push(reason => {
    searchParams.entityType.properties.forEach(propInfo => {
      if (!searchParams.propertyValues[propInfo.name]) return; // Ignore empty values
      if (fieldForProperty[propInfo.name]) return; // Ignore already existing fields. Change listeners will update existing fields.
      var criteriaE = cE(summaryLineE, "hbox", "criteria");
      var labelE = cE(criteriaE, "div", "fieldLabel");
      labelE.textContent = propInfo.label;
      var clearE = cE(criteriaE, "hbox", "clearCriteria");
      var iconE = cE(clearE, "img", null, {
          //nosrc: "icons/clear.png",
          alt: "x",
          title: "Remove criteria",
          width: "16px",
          height: "16px",
      });
      clearE.addEventListener("click", event => {
        removeElement(fieldForProperty[propInfo.name]);
        fieldForProperty[propInfo.name] = null;
        searchParams.propertyValues[propInfo.name] = null;
        searchParams.fireChangeObservers("user");
      }, false);
      var field = newFieldForProperty(searchParams, propInfo);
      if (!field) return; // TODO remove once we support all types
      criteriaE.insertBefore(field.getElement(), clearE);
      fieldForProperty[propInfo.name] = criteriaE;
    });
  });
}

function addButtons(summaryType) {
  // Search field buttons
  E("simpleSearchButton").addEventListener("click", startSimpleSearch, false);
  E("extendedSearchButton").addEventListener("click", startExtendedSearch, false);
  E("simpleSearchClear").addEventListener("click", event => {
    E("simpleSearchField").value = "";
    E("simpleSearchField").focus();
  }, false);

  // Action bar buttons
  var contextE = E("footerContext");
  var globalE = E("footerGlobal");
  var openButtonE = createBigButton(contextE, "Open", "icons/open.png", event => {
    gSelectedItems.forEach(openEntityPage);
  }, errorCritical);
  /*var exportButtonE = createBigButton(contextE, "Export", "icons/export.png", event => {
    export(gSelectedItems);
  }, errorCritical);*/
  var newButtonE = createBigButton(globalE, "New", "icons/plus.png", event => {
    openEntityPage(summaryType.newInstance(0));
  }, errorCritical);

  // Selector checkboxen
  selectedItemsChanged = selectedItems => {
    gSelectedItems = selectedItems;
    if (selectedItems.length) {
      openButtonE.removeAttribute("disabled");
    } else {
      openButtonE.setAttribute("disabled", "disabled");
    }
  };
  selectedItemsChanged([]);

  // View mode switcher
  E("gridView").addEventListener("click", event => {
    gViewMode = 1;
    showSearchResults();
  }, false);
  E("cardView").addEventListener("click", event => {
    gViewMode = 2;
    showSearchResults();
  }, false);
  E("mapView").addEventListener("click", event => {
    gViewMode = 3;
    showSearchResults();
  }, false);
}


function startSimpleSearch() {
  assert(!gIsExtendedSearch, "Not in simple search mode");
  var simpleSearch = E("simpleSearchField").value;
  if (!simpleSearch) {
    return; // if the user didn't enter any search term and hits enter, do nothing
  }
  if (!gSearchParams) return; // happens during load
  showSearchRunning();

  gSearchParams.fetchSimple(simpleSearch, 201, showSearchResults, errorCritical);
}

function startExtendedSearch() {
  assert(gIsExtendedSearch, "Not in extended search mode");
  if (!gSearchParams) return; // happens during load
  showSearchRunning();

  gSearchParams.fetch(201, showSearchResults, errorCritical);
}

/**
 * @param results {Array of SummaryType}
 */
function showSearchResults(results) {
  if (results) {
    gResults = results;
  } else {
    results = gResults;
  }

  if (results.length == 1) {
    var query = parseURLQueryString(window.location.hash.replace("#", ""));
    if (query.openDirectly && sanitize.boolean(query.openDirectly)) {
      openEntityPage(results[0], true);
      return;
    }
  }

  clearSearchResults();
  showSearchHeader(results);

  if (results.length) {
    if (gViewMode == 1) {
      showGridSearchResults(results);
    } else if (gViewMode == 2) {
      showCardSearchResults(results);
    } else if (gViewMode == 3) {
      showMapSearchResults(results);
    } else {
      throw NotReached();
    }
  }
}

/**
 * @param results {Array of SummaryType}
 */
function showGridSearchResults(results) {
  removeElement(E("gridResults"));
  var grid = new EntityGrid(gSearchParams.resultType, results, false, true,
      selectedItems => selectedItemsChanged(selectedItems));
  E("results").insertBefore(grid.getElement(), E("cardResults"));
  grid.tableE.id = "gridResults";
  grid.theadE.id = "gridHead";
  grid.tbodyE.id = "gridContents";
}

/**
 * @param results {Array of SummaryType}
 */
function showCardSearchResults(results) {
  var type = gSearchParams.resultType;
  var resultsE = E("cardResults");
  cleanElement(resultsE);
  var selectedItems = [];

  results.forEach(result => {
    var cardE = cE(resultsE, "hwrap", "card");
    cardE.addEventListener("click", event => {
      openEntityPage(result);
    }, false);
    // selector checkboxen
    var selectItemE = cE(cardE, "input", "selectItem", { type : "checkbox" });
    selectItemE.addEventListener("click", event => {
      event.stopPropagation(); // don't bubble - it would open the item
      if (selectItemE.checked) {
        selectedItems.push(result);
      } else {
        arrayRemove(selectedItems, result);
      }
      selectedItemsChanged(selectedItems);
    }, false);
    var displayOrder = propInfo => {
      // strings first, then long fields, then short fields
      switch (propInfo.type.name) {
        case "string":
          return 1;
        case "int":
        case "decimal":
        case "boolean":
        case "date":
        case "datetime":
          return 3;
        default:
          return 2;
      }
    };
    type.properties.sort((a, b) => {
      return displayOrder(a) - displayOrder(b);
    }).forEach(propInfo => {
      var labeledFieldE = cE(cardE, "vbox", "labeledField", { datatype : propInfo.type.name });
      var labelE = cE(labeledFieldE, "hbox", "label");
      labelE.textContent = gApp.strings.getLabelForProperty(type, propInfo.name) || "\xA0";
      var fieldE = cE(labeledFieldE, "hbox", "field", { datatype : propInfo.type.name });
      // Data
      var value = result.propertyValues[propInfo.name];
      fieldE.textContent = propInfo.type.displayShort(value) || "\xA0";
    });
  });
}

/**
 * Search is ongoing. Reflect that in the UI.
 */
function showSearchRunning() {
  var countText = "Searching {0}...".replace("{0}", gApp.strings.getLabelForType(gSearchParams.resultType));
  E("resultCount").textContent = countText;
  E("gridResults").setAttribute("hidden", "true");
  E("cardResults").setAttribute("hidden", "true");
  E("mapResults").setAttribute("hidden", "true");
  clearSearchResults();
}

function showSearchHeader(results) {
  var countText = "All Results ({0})".replace("{0}", results.length);
  if (results.length == 0) {
    countText = "No results";
  } else if (results.length > 200) {
    countText = "We found a lot of matches. Here are the first {0}.".replace("{0}", 200);
  }
  E("resultCount").textContent = countText;

  E("gridView").setAttribute("selected", gViewMode == 1 ? "true" : "false");
  E("cardView").setAttribute("selected", gViewMode == 2 ? "true" : "false");
  E("mapView").setAttribute("selected", gViewMode == 3 ? "true" : "false");

  if (gViewMode == 1) {
    E("gridResults").removeAttribute("hidden");
  } else {
    E("gridResults").setAttribute("hidden", "true");
  }
  if (gViewMode == 2) {
    E("cardResults").removeAttribute("hidden");
  } else {
    E("cardResults").setAttribute("hidden", "true");
  }
  if (gViewMode == 3) {
    E("mapResults").removeAttribute("hidden");
  } else {
    E("mapResults").setAttribute("hidden", "true");
  }
}

function clearSearchResults() {
  cleanElement(E("gridResults"));
  cleanElement(E("cardResults"));
  selectedItemsChanged([]);
}



/**
 * @param summaryInstance {EntityInstance}
 * @param sameTab {Boolean} Optional, default false
 *    if true, replace the search page with the entity page.
 *    if false or null, open the entity page in a new tab
 */
function openEntityPage(summaryInstance, sameTab) {
  assert(summaryInstance.entityType instanceof SummaryType);
  var entityFullName = strRemoveEnd(summaryInstance.entityType.fullName, "Summary");
  var url = "entity.html#fullname=" + entityFullName + "&id=" + summaryInstance.id;
  if (sameTab) {
    window.location = url;
  } else {
    openTab(url);
  }
}

/**
 * Call this when we loaded the type and all resources.
 * You need to call this only once.
 * @param summaryType {SummaryType}
 */
function setTitleFromEntity(summaryType) {
  assert(summaryType instanceof SummaryType);
  var typeLabel = gApp.strings.getLabelForType(summaryType);
  document.title = typeLabel + " - " + gApp.strings.appName;
  //E("title").textContent = typeLabel;
}

/**
 * Just a quick default, before the server fetches.
 * @param fullName {String}
 */
function setTitleFromFullName(fullName) {
  sanitize.label(fullName);
  var typeLabel = typeNameReadable(fullName);
  document.title = typeLabel + " - " + gApp.strings.appName;
  //E("title").textContent = typeLabel;
}

function toggleExtendedSearch() {
  gIsExtendedSearch = !gIsExtendedSearch;
  if (gIsExtendedSearch) {
    E("extendedSearchBox").removeAttribute("hidden");
    E("simpleSearchBar").setAttribute("hidden", "true");
  } else {
    E("extendedSearchBox").setAttribute("hidden", "true");
    E("simpleSearchBar").removeAttribute("hidden");
    E("simpleSearchField").focus();
  }
}
