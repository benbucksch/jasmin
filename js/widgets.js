
/**
 * Creates a UI widget appropriate to represent and edit this property.
 * @returns {DataField}
 */
function newFieldForProperty(entityInstance, propInfo) {
  if (propInfo.type.name == "string") { // == stringType) {
    if (propInfo.Length && sanitize.integer(propInfo.Length) > 300) {
      return new MultilineField(entityInstance, propInfo);
    } else {
      return new StringField(entityInstance, propInfo);
    }
  } else if (propInfo.type.name == "int") { // == integerType) {
    return new IntegerField(entityInstance, propInfo);
  } else if (propInfo.type.name == "decimal") { // == decimalType) {
    return new DecimalField(entityInstance, propInfo);
  } else if (propInfo.type.name == "boolean") { // == booleanType) {
    return new BooleanField(entityInstance, propInfo);
  } else if (propInfo.type.name == "date") { // == dateType) {
    return new DateField(entityInstance, propInfo);
  } else if (propInfo.type.name == "datetime") { // == dateTimeType) {
    return new DateTimeField(entityInstance, propInfo);
  } else if (propInfo.type instanceof EnumType) {
    return new EnumDropdownField(entityInstance, propInfo);
  } else if (propInfo.type.typeName == "Address") {
    // TODO Address widget
  } else if (propInfo.type instanceof EntityType) {
    if (propInfo.isInteger) {
      return new EntityDropdownField(entityInstance, propInfo);
    } else {
      return new InlineSearchField(entityInstance, propInfo);
    }
  } else {
    // TODO EnumType
    // TODO other types
    // throw NotReached("No field for property type " + propInfo.type.fullName);
  }
}


function DataField(entityInstance, propInfo) {
  assert(entityInstance instanceof EntityInstance);
  assert(typeof(propInfo) == "object" && typeof(propInfo.name) == "string");
  this.entityInstance = entityInstance;
  this.propInfo = propInfo;
}
DataField.prototype = {
  /**
   * {EntityInstance}
   */
  entityInstance : null,
  /**
   * @see EntityType.properties
   */
  propInfo : null,

  /**
   * What the field content should go back to when the entity property is null.
   * Subclasses must override this with a suitable default for their data type.
   */
  defaultValue : null,

  _getEntityValue : function() {
    return this.entityInstance.propertyValues[this.propInfo.name] || this.defaultValue;
  },

  _setEntityValue : function(val) {
    this.entityInstance.propertyValues[this.propInfo.name] = val;
  },

  _dataToField : function() {
    if (this._getEntityValue() == this._getFieldValue()) {
      return;
    }
    this._setFieldValue(this._getEntityValue());
  },

  _fieldToData : function() {
    this._setEntityValue(this._getFieldValue());
    this.entityInstance.fireChangeObservers("user");
  },

  _hookupObserver : function() {
    this.entityInstance.changeObservers.push(reason => {
      this._dataToField();
    });
  },

  /**
   * Creates the HTML widget
   *
   * @returns {DOMElement}
   */
  getElement : function(parentE) {
    throw NotImplemented();
  },

  _getFieldValue : function() {
    throw NotImplemented();
  },

  _setFieldValue : function() {
    throw NotImplemented();
  },
}

/**
 * Abstract base class for all text or number input fields.
 */
function InputField(entityInstance, propInfo) {
  DataField.call(this, entityInstance, propInfo);
}
InputField.prototype = {
  _inputE : null,

  defaultValue : "",

  getElement : function() {
    this._inputE = cE(null, "input");
    if (this.propInfo.required) {
      this._inputE.setAttribute("required", "true");
    }
    if (this.propInfo.readonly) {
      this._inputE.setAttribute("readonly", "true");
    }
    this._inputE.addEventListener("input", event => {
      this._fieldToData();
    }, false);
    this._hookupObserver();
    this._dataToField();
    return this._inputE;
  },

  _getFieldValue : function() {
    return this._inputE.value;
  },

  _setFieldValue : function(value) {
    this._inputE.value = value;
  },
}
extend(InputField, DataField);

function StringField(entityInstance, propInfo) {
  InputField.call(this, entityInstance, propInfo);
}
StringField.prototype = {
  getElement : function() {
    var inputE = InputField.prototype.getElement.call(this);
    if (this.propInfo.Length) {
      this._inputE.setAttribute("maxlength", sanitize.integer(this.propInfo.Length));
    }
    return inputE;
  },
}
extend(StringField, InputField);

function NumberField(entityInstance, propInfo) {
  InputField.call(this, entityInstance, propInfo);
}
NumberField.prototype = {
  defaultValue : 0,

  getElement : function() {
    var result = InputField.prototype.getElement.call(this);
    this._inputE.setAttribute("type", "number");
    if (this.propInfo.Minimum) {
      this._inputE.setAttribute("min", sanitize.integer(this.propInfo.Minimum));
    }
    if (this.propInfo.Maximum) {
      this._inputE.setAttribute("max", sanitize.integer(this.propInfo.Maximum));
    }
    return result;
  },
}
extend(NumberField, InputField);

function IntegerField(entityInstance, propInfo) {
  NumberField.call(this, entityInstance, propInfo);
}
IntegerField.prototype = {
  _getFieldValue : function() {
    return parseInt(this._inputE.value);
  },
}
extend(IntegerField, NumberField);

function DecimalField(entityInstance, propInfo) {
  NumberField.call(this, entityInstance, propInfo);
}
DecimalField.prototype = {
  _getFieldValue : function() {
    return parseFloat(this._inputE.value);
  },
}
extend(DecimalField, NumberField);


function BooleanField(entityInstance, propInfo) {
  DataField.call(this, entityInstance, propInfo);
}
BooleanField.prototype = {
  _inputE : null,

  defaultValue : false,

  getElement : function() {
    this._inputE = cE(null, "input", null, { type : "checkbox" });
    if (this.propInfo.readonly) {
      this._inputE.setAttribute("disabled", "true");
    }
    // change event fires too late, data might not be up to date.
    // <http://stackoverflow.com/questions/4471401/getting-value-of-html-checkbox-from-onclick-onchange-events>
    this._inputE.addEventListener("click", event => {
      this._fieldToData();
    }, false);
    this._hookupObserver();
    this._dataToField();
    return this._inputE;
  },

  _getFieldValue : function() {
    return this._inputE.checked;
  },

  _setFieldValue : function(bool) {
    this._inputE.checked = bool;
  },
}
extend(BooleanField, DataField);


function DateField(entityInstance, propInfo) {
  InputField.call(this, entityInstance, propInfo);
}
DateField.prototype = {
  defaultValue : null,

  getElement : function() {
    var result = InputField.prototype.getElement.call(this);
    this._inputE.setAttribute("type", "date");
    return result;
  },

  _getFieldValue : function() {
    // TODO return Date.parse(this._inputE.value);
    return this._inputE.value;
  },

  _setFieldValue : function(date) {
    this.propInfo.type.display(date);
  },
}
extend(DateField, InputField);

function DateTimeField(entityInstance, propInfo) {
  DateField.call(this, entityInstance, propInfo);
}
DateTimeField.prototype = {
  getElement : function() {
    var result = DateField.prototype.getElement.call(this);
    this._inputE.setAttribute("type", "datetime");
    return result;
  },
}
extend(DateTimeField, DateField);

/**
 * Like StringField, but using <textarea> for multiple text lines
 * instead of <input type="text">.
 * Useful for "Notes" fields.
 */
function MultilineField(entityInstance, propInfo) {
  DataField.call(this, entityInstance, propInfo);
}
MultilineField.prototype = {
  _textE : null,

  defaultValue : "",

  getElement : function() {
    this._textE = cE(null, "textarea");
    this._textE.setAttribute("rows", 3);
    if (this.propInfo.required) {
      this._textE.setAttribute("required", "true");
    }
    if (this.propInfo.readonly) {
      this._textE.setAttribute("readonly", "true");
    }
    if (this.propInfo.Length && sanitize.integer(this.propInfo.Length)) {
      this._textE.setAttribute("maxlength", this.propInfo.Length);
    }

    this._textE.addEventListener("input", event => {
      this._fieldToData();
    }, false);
    this._hookupObserver();
    this._dataToField();
    return this._textE;
  },

  _getFieldValue : function() {
    return this._textE.value;
  },

  _setFieldValue : function(text) {
    this._textE.value = text;
  },
}
extend(MultilineField, DataField);



function EnumDropdownField(entityInstance, propInfo) {
  DataField.call(this, entityInstance, propInfo);
  assert(propInfo.type instanceof EnumType);
  assert(propInfo.type._loaded > 0, "EnumType is not loaded");
}
EnumDropdownField.prototype = {
  _dropdownE : null,
  _nullOptionE : null, // <option> element selected when no value should be set
  defaultValue : null,

  getElement : function() {
    this._dropdownE =  cE(null, "select", "enum");
    if (this.propInfo.required) {
      this._dropdownE.setAttribute("required", "true");
    }
    if (this.propInfo.readonly) {
      this._dropdownE.setAttribute("disabled", "true");
    }

    if (!this.propInfo.required &&
        this.propInfo.ClientNullable != "true") {
      this._nullOptionE = cE(this._dropdownE, "option", "null");
      this._nullOptionE.textContent = " ";
      this._nullOptionE.option = null;
    }

    mapToArray(this.propInfo.type.options).forEach(option => {
      var optionE = cE(this._dropdownE, "option");
      optionE.textContent = this.propInfo.type.display(option);
      optionE.value = option.value;
      optionE.option = option;
    });

    this._dropdownE.addEventListener("change", event => {
      this._fieldToData();
    }, false);
    this._hookupObserver();
    this._dataToField();
    return this._dropdownE;
  },

  _getFieldValue : function() {
    var optionE = this._dropdownE.selectedOptions[0];
    return optionE ? optionE.option : null;
  },

  _setFieldValue : function(option) {
    nodeListToArray(this._dropdownE.options).forEach(optionE => {
      optionE.selected = optionE.option == option; // works for null, too
    });
  },
}
extend(EnumDropdownField, DataField);


function EntityDropdownField(entityInstance, propInfo) {
  DataField.call(this, entityInstance, propInfo);
  assert(propInfo.type instanceof EntityType);
  //assert(propInfo.type._loaded > 0, "EntityType is not loaded");
}
EntityDropdownField.prototype = {
  _dropdownE : null,
  _nullOptionE : null, // <option> element selected when no value should be set
  defaultValue : null,

  getElement : function() {
    this._dropdownE =  cE(null, "select", "entity");
    if (this.propInfo.required) {
      this._dropdownE.setAttribute("required", "true");
    }
    if (this.propInfo.readonly) {
      this._dropdownE.setAttribute("disabled", "true");
    }

    gApp.fetchReferences(this.propInfo.type, entries => {
      this._populate(entries);
    }, errorCritical);

    this._dropdownE.addEventListener("change", event => {
      this._fieldToData();
    }, false);
    this._hookupObserver();
    this._dataToField();
    return this._dropdownE;
  },

  /**
   * @param entries {Array of EntityInstance}
   */
  _populate : function(entries) {
    if (!this.propInfo.required &&
        this.propInfo.ClientNullable != "true") {
      this._nullOptionE = cE(this._dropdownE, "option", "null");
      this._nullOptionE.textContent = " ";
      this._nullOptionE.entityInstance = null;
    }
    entries.forEach(entityInstance => {
      var optionE = cE(this._dropdownE, "option");
      optionE.textContent = this.propInfo.type.display(entityInstance);
      optionE.entityInstance = entityInstance;
      optionE.selected = false;
    });
  },

  _getFieldValue : function() {
    var optionE = this._dropdownE.selectedOptions[0];
    return optionE ? optionE.entityInstance : null;
    //var jsonValue = this.propInfo.type.instanceToJSON(this._getEntityValue()); // will be integer, as expected
  },

  _setFieldValue : function(entityInstance) {
    nodeListToArray(this._dropdownE.options).forEach(optionE => {
      optionE.selected = optionE.entityInstance == entityInstance; // works for null, too
    });
  },
}
extend(EntityDropdownField, DataField);



function InlineSearchField(entityInstance, propInfo) {
  DataField.call(this, entityInstance, propInfo);
}
InlineSearchField.prototype = {
  _inputE : null,
  _autocomplete : null,
  _source : null,
  /**
   * The item in the dropdown that the user selected.
   * {SummaryInstance}
   */
  _selectedEntityInstance : null,
  defaultValue : null,

  getElement : function(parentE) {
    var boxE = cE(null, "hbox", "inlineSearch");
    this._inputE = cE(boxE, "input");
    if (this.propInfo.readonly) {
      this._inputE.setAttribute("readonly", "true");
    }
    this._inputE.placeholder = "Search " + gApp.strings.getLabelForType(this.propInfo.type);

    // Autocomplete widget, @see lib/AutoComplete.js
    this._autocomplete = new AutocompleteWidget(this._inputE, {
      defaultAction : item => this._itemSelected(item),
      onActionCopyText : false,
      delay : 200,
      parent : boxE,
    });
    this.propInfo.type.fetchType(() => {
      this.propInfo.type.fetchPropertyTypes(() => {
        this._source = new InlineSearchSource(this.propInfo.type, this._autocomplete);
        this._autocomplete.addSource(this._source);
      }, errorNonCritical); // TODO fails for some types due to not finding the type in file
    }, errorCritical);

    this._inputE.addEventListener("blur", event => { // on leaving field
      // If user typed, but didn't select anything, delete it
      this._setFieldValue(this._getEntityValue());
    }, false);

    //this._addButtons(boxE);
    this._hookupObserver();
    this._dataToField();
    return boxE;
  },

  // TODO style to put them inside input field.
  // Need to re-style box instead of input field.
  _addButtons : function(boxE) {
    var clearButtonE = cE(boxE, "img", "clear", {
        src: "icons/clear.png",
        alt: "Clear",
        title: "Clear field",
        width: "12",
        height: "12",
    });
    clearButtonE.addEventListener("click", event => {
      this._setEntityValue(null);
      this.entityInstance.fireChangeObservers("user");
      //this._setFieldValue(null); // happens via observer
    }, false);

    var infoE = cE(boxE, "img", "clear", {
        src: "icons/search-info.png",
        alt: "Info",
        width: "12",
        height: "12",
    });
    infoE.title = gApp.strings.get(gApp.strings.getResourceNameForType(this.propInfo.type) + "Inline_InfoContent");
  },

  _getFieldValue : function() {
    return this._selectedEntityInstance;
  },

  _setFieldValue : function(entityInstance) {
    this._selectedEntityInstance = entityInstance;
    this._inputE.value = entityInstance ? entityInstance.title() : "";
    if (this._source) {
      this._source.cancel();
    }
  },

  _fieldToData : function() {
    this._setEntityValue(this._selectedEntityInstance);

    var id = this._selectedEntityInstance ? this._selectedEntityInstance.id : null;
    if (this.propInfo.IsSummaryFor && sanitize.identifier(this.propInfo.IsSummaryFor)) {
      this.entityInstance.propertyValues[this.propInfo.IsSummaryFor] = id;
    }
    if (this.propInfo.IsClientSummaryFor && sanitize.identifier(this.propInfo.IsClientSummaryFor)) {
      this.entityInstance.propertyValues[this.propInfo.IsClientSummaryFor] = id;
    }
  },

  /**
   * @params acItem {InlineSearchItem} The user selected this item from the dropdown
   */
  _itemSelected : function(acItem) {
    assert(acItem instanceof InlineSearchItem);
    this._setFieldValue(acItem.summaryInstance);
    this._fieldToData();
  }
}
extend(InlineSearchField, DataField);

/**
 * @param summaryType {SummaryType}
 * @param ac {AutocompleteWidget}
 */
function InlineSearchSource(summaryType, ac) {
  assert(summaryType instanceof SummaryType);
  assert(ac instanceof AutocompleteWidget);
  this._ac = ac;
  this._search = new SearchRequest(summaryType);
}
InlineSearchSource.prototype = {
  _fetch : null,
  _ac : null,

  onTextChanged : function(text) {
    this.cancel();
    this._ac.hideSuggestions();
    this._ac.clearItems();
    if (!text) return;
    this._ac.addItem(new InlineSearchHeader(this._search.resultType));
    this._ac.shouldShowSuggestions(); // opens dropdown once the first items arrive
    this._fetch = this._search.fetchSimple(text, 25, results => {
      results.forEach(result => {
        this._ac.addItem(new InlineSearchItem(result));
      });
    }, ex => {
      if (ex.causedByUser) return;
      errorNonCritical(ex);
      // show error inline as item
      // params: value, label, description, icon, action, nonSelectable, highlightedText, classes
      this._ac.clearItems();
      this._ac.addItem(new SimpleAutocompleteItem(null, ex, null, null, null /* TODO icon */, true, null, ["error"]));
    });
  },

  cancel : function() {
    if (this._fetch) {
      this._fetch.cancel();
    }
  },
};
extend(InlineSearchSource, AutocompleteSource);

function InlineSearchItem(summaryInstance) {
  this.summaryInstance = summaryInstance;
}
InlineSearchItem.prototype = {
  summaryInstance : null,

  createElement : function() {
    var rowE = cE(null, "hbox", "row");
    this.summaryInstance.entityType.properties.forEach(propInfo => {
      var cellE = cE(rowE, "hbox", "cell", { datatype : _getTypeProperty(propInfo.type) });
      // Data
      var value = this.summaryInstance.propertyValues[propInfo.name];
      cellE.textContent = propInfo.type.displayShort(value);
    });
    return rowE;
  },
};
extend(InlineSearchItem, AutocompleteItem);

function InlineSearchHeader(summaryType) {
  this._summaryType = summaryType;
}
InlineSearchHeader.prototype = {
  _summaryType : null,

  createElement : function() {
    var rowE = cE(null, "hbox", "header");
    this._summaryType.properties.forEach(propInfo => {
      var thE = cE(rowE, "hbox", "cell", { datatype : _getTypeProperty(propInfo.type) });
      thE.textContent = gApp.strings.getLabelForProperty(this._summaryType, propInfo.name);
      return thE;
    });
    return rowE;
  },
};
extend(InlineSearchHeader, AutocompleteItem);



function _getTypeProperty(type) {
  if (type instanceof EntityType) {
    return "entity";
  } else if (type instanceof EnumType) {
    return "enum";
  }
  return type.name;
}

/**
 * @param type {EntityType} All items have this type.
 * @param items {Array of EntityInstance} 
 * @param editable {Boolean} If false, just show strings in cells.
 *   If true, add widgets for every cell (slow).
 * @param withSelectors {Boolean} Add selection checkboxen as first column.
 *   They allow multiple selection of items.
 * @param selectedItemsCallback {Function(selectedItems {Array of EntityInstance})}
 *   Will be called each time when the selection changes (added or removed items).
 */
function EntityGrid(type, items, editable, withSelectors, selectedItemsCallback) {
  assert(Array.isArray(items));
  assert(typeof(editable) == "boolean");
  assert(typeof(withSelectors) == "boolean");
  assert(typeof(selectedItemsCallback) == "function");

  this.tableE = cE(null, "table", "grid");
  this.theadE = cE(this.tableE, "thead");
  this.tbodyE = cE(this.tableE, "tbody", "click");
  this.selectedItems = [];
  this._selectorCheckboxEs = {};
  if (editable) {
    this.tableE.classList.add("edit");
  }

  // Header
  var headerRowE = cE(this.theadE, "tr");
  if (withSelectors) {
    var selectorHeaderE = cE(headerRowE, "th", "selector");
    var selectAllE = cE(selectorHeaderE, "input", "selectAll", { type : "checkbox" });
    selectAllE.addEventListener("click", event => {
      if (selectAllE.checked) {
        this.selectedItems = items;
      } else {
        this.selectedItems = [];
      }
      mapToArray(this._selectorCheckboxEs).forEach(checkboxE => checkboxE.checked = selectAllE.checked);
      selectedItemsCallback(this.selectedItems);
    }, false);
  }
  type.properties.forEach(propInfo => {
    var thE = cE(headerRowE, "th", null, { datatype : _getTypeProperty(propInfo.type) });
    thE.textContent = gApp.strings.getLabelForProperty(type, propInfo.name);
  });

  // Data rows
  items.forEach(item => {
    var rowE = cE(this.tbodyE, "tr");
    if (!editable) {
      rowE.addEventListener("click", event => {
        openEntityPage(item);
      }, false);
    }
    if (withSelectors) {
      var selectorCellE = cE(rowE, "td", "selector");
      var selectItemE = cE(selectorCellE, "input", "selectItem", { type : "checkbox" });
      selectItemE.addEventListener("click", event => {
        event.stopPropagation(); // don't bubble - it would open the item
        if (selectItemE.checked) {
          this.selectedItems.push(item);
        } else {
          arrayRemove(this.selectedItems, item);
        }
        selectedItemsCallback(this.selectedItems);
      }, false);
      this._selectorCheckboxEs[item.id] = selectItemE;
    }
    type.properties.forEach(propInfo => {
      var cellE = cE(rowE, "td", editable ? "edit" : "display", { datatype : _getTypeProperty(propInfo.type) });
      // Data
      if (editable) {
        cellE.appendChild(newFieldForProperty(item, propInfo).getElement());
      } else {
        var value = item.propertyValues[propInfo.name];
        if (propInfo.type.name == "boolean") {
          if (value) {
            cE(cellE, "img", "checked", {
              src: "icons/checked-20.png",
              alt: "Yes",
              title: "Yes",
              height: "20",
              width: "20",
            });
          }
        } else {
          cellE.textContent = propInfo.type.displayShort(value);
        }
      }
    });
  });
}
EntityGrid.prototype = {
  // <table> {DOMElement}
  tableE : null,
  // <thead> {DOMElement}
  theadE : null,
  // <tbody> {DOMElement}
  tbodyE : null,

  /**
   * Items that the user selected using the selector checkboxen
   * {Array of EntityInstance}
   */
  selectedItems : null,

  /**
   * {Map id of entity instance {String} -> selector checkbox {DOMElement}}
   */
  _selectorCheckboxEs : null,

  getElement : function() {
    return this.tableE;
  },
}


/**
 * Creates the label for a form field.
 * @param label {String} user-readable, translated string that describes the field
 * @param field {DataField}
 * @returns {DOMElement}
 */
function LabeledField(label, field) {
  label = sanitize.label(label) || " ";
  assert(field instanceof DataField);
  var propInfo = field.propInfo;

  this._containerE = cE(null, "divi", "fieldContainer", { datatype: _getTypeProperty(field.propInfo.type) });
  var vboxE = cE(this._containerE, "vbox");
  var labelE = cE(vboxE, "div", "fieldLabel");
  var textE = cE(labelE, "span");
  textE.textContent = label;
  this._errorMessageE = cE(labelE, "span", "error");
  if (field.propInfo.required) {
    cE(labelE, "span", "required").textContent = "°";
  }
  if (field.propInfo.readonly) {
    this._containerE.classList.add("readonly");
  }
  this.field = field;
  this.fieldE = field.getElement();
  vboxE.appendChild(this.fieldE);
}
LabeledField.prototype = {
  /**
   * {DataField}
   */
  field : null,

  /**
   * The DOM element generated by _field.
   * May be the <input>, but might be a box around it.
   * {DOMElement}
   */
  fieldE : null,

  /**
   * Where the validation error message will be shown
   * {DOMElement}
   */
  _errorMessageE : null,

  // @see element getter
  _containerE : null,

  /**
   * The label and field together as DOM tree.
   * {DOMElement}
   */
  get element() {
    return this._containerE;
  },


  /**
   * If there is a validation error on this field,
   * which is returned from the server,
   * set it here. It will show the field in red and
   * show the error message around the field.
   * This is sufficient - do not also show a model error dialog.
   *
   * The error will be cleared once the user changes the field.
   */
  setError : function(errorMessage) {
    this._containerE.classList.add("error");
    this._errorMessageE.textContent = sanitize.label(errorMessage);

    if (this._fieldE.setCustomValidity) {
      this._fieldE.setCustomValidity(errorMessage);
    }
  },

  clearError : function() {
    cleanElement(this._errorMessageE);
    this._containerE.classList.remove("error");

    if (this._fieldE.setCustomValidity) {
      this._fieldE.setCustomValidity("");
    }
  },
}


/**
 * @param parentE {DOMElement}
 * @param label {String} user-readable label underneath the button
 * @param imgFile {URL as String} URL of the image src file,
 *     relative to the root of Jasmin JS
 *     E.g. "icons/save.png"
 * @param clickCallback {Function(event {DOMEvent})} Called when the user presses the button
 * @param errorCallback   Called when clickCallback() throws
 */
function createBigButton(parentE, label, imgFile, clickCallback, errorCallback) {
  /*  <vbox id="save" class="bigbutton">
        <hbox class="circle">
          <img src="icons/save.png" alt="Save" title="Save" width="16" height="16"></img>
        </hbox>
        <hbox class="label">Save</hbox>
      </vbox>  */
  var buttonE = cE(parentE, "vbox", "bigbutton");
  var circleE = cE(buttonE, "hbox", "circle");
  var imgE = cE(circleE, "img", null, {
      src: imgFile,
      title: label,
      alt: label,
      width: "16",
      height: "16",
  });
  var labelE = cE(buttonE, "hbox", "label");
  labelE.textContent = label;

  buttonE.addEventListener("click", event => {
    try {
      clickCallback(event);
    } catch (ex) {
      errorCallback(ex);
    }
  }, false);
  return buttonE;
}

/**
 * Creates a group of tabs, including tab headers and tab content panes.
 * You need to add tabHeaderE and tabContentPanesE to your DOM.
 */
function Tabs() {
  this._tabHeadersE = cE(null, "hbox", "tabHeaders");
  this._tabContentPanesE = cE(null, "vbox", "tabContentPanes");
}
Tabs.prototype = {
  _tabHeadersE : null,
  _tabContentPanesE : null,
  _count : 0,

  /**
   * This is the tab header bar.
   * You need to add this to your page DOM.
   * {DOMElement}
   */
  get tabHeadersE() {
    return this._tabHeadersE;
  },

  /**
   * This is collection of tab contents.
   * You need to add this to your page DOM
   * where you want the content to appear.
   * {DOMElement}
   */
  get tabContentPanesE() {
    return this._tabContentPanesE;
  },

  /**
   * Call this to add a new tab.
   *
   * @param title {String} user-readable label added as tab header
   * @param contentPaneE {DOMElement} content pane that you want
   *     displayed when the user clicks the tab.
   *     That's usually an <hbox> or vbox>.
   * @returns tabID {Integer} tab handle you can use to show or remove this tab later.
   */
  addTab(title, contentPaneE) {
    var tabID = ++this._count; // 1-based

    var headerE = cE(this._tabHeadersE, "div", "tabHeader", {
      tabID : tabID,
      selected : false,
    });
    headerE.classList.add("click");
    headerE.textContent = sanitize.label(title);
    headerE.addEventListener("click", event => {
      this.showTab(tabID);
    }, false);

    contentPaneE.setAttribute("tabID", tabID);
    contentPaneE.setAttribute("selected", false);
    this._tabContentPanesE.appendChild(contentPaneE);

    if (tabID == 1) {
      this.showTab(1);
    }
    return tabID;
  },

  showTab : function(tabID) {
    tabID = sanitize.integer(tabID);
    assert(tabID <= this._count);
    var selectChildElementsByID = function(parentE, id) {
      nodeListToArray(parentE.childNodes).forEach(childE => {
        childE.setAttribute("selected",
            parseInt(childE.getAttribute("tabID")) == id);
      });
    }
    selectChildElementsByID(this._tabHeadersE, tabID);
    selectChildElementsByID(this._tabContentPanesE, tabID);
    // show/hide for content panes, and selection for headers will be done in CSS.
  },
}
