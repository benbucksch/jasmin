var gApp = null; // {Application} singleton
var gSummaryNamesCache = []; // {Array of String} Cache of gApp.getSummariesList(), but only the typeName

function onLoad() {
  var starttime = new Date();
  gApp = new Application(() => {
    //loadAllResources(() => {
      buildStartPage();
      buildQuickSearchField();
      ddebug("Startup took " + (new Date() - starttime) + "ms");
    //});
  }, errorCritical);
}
window.addEventListener("DOMContentLoaded", onLoad, false);

function buildStartPage() {
  document.title = gApp.strings.appName;
  E("title").textContent = gApp.strings.appName;
  var tabsE = E("tabs");
  var tabs = {}; // Map Module.Submodule {String} -> tabContentE {DOMElement}
  gApp.getSummariesList().sort((a, b) => { // TODO sort translated, not type
    var aParts = a.split(".");
    var bParts = b.split(".");
    var aModule = aParts[0];
    var bModule = bParts[0];
    var aSubmodule = aParts[0] + "." + bParts[1];
    var bSubmodule = bParts[0] + "." + bParts[1];
    if (aModule < bModule) return -1;
    if (aModule > bModule) return 1;
    if (aSubmodule < bSubmodule) return -1;
    if (aSubmodule > bSubmodule) return 1;
    if (strContains(a, "Reference") && !strContains(b, "Reference")) return 1;
    if (strContains(b, "Reference") && !strContains(a, "Reference")) return -1;
    var aEntry = a.split(".").pop();
    var bEntry = b.split(".").pop();
    if (aEntry < bEntry) return -1;
    if (aEntry > bEntry) return 1;
    return 0;
  }).forEach(fullName => {
    var nameParts = fullName.split(".");
    var submodule = nameParts[0];
    //var submodule = nameParts[0] + "." + nameParts[1];
    if (!tabs[submodule]) {
      var tabE = cE(tabsE, "tab");
      var tabHeaderE  = cE(tabE, "div", "header");
      var tabContentE  = cE(tabE, "vbox", "content");
      /*gApp.strings.getLabelForType(submodule, label => {
        tabHeaderE.textContent = label;
        gApp.strings.getLabelForType(fullName + "Search_DisplayName", label => {
          entryE.textContent = label;
        }, errorNonCritical);
      }, errorNonCritical);*/
      tabHeaderE.textContent =
          gApp.strings.getLabelForType({ fullName : submodule }) ||
          capitalizeWords(typeNameReadable(submodule));
      tabs[submodule] = tabContentE;
    }
    var contentE = tabs[submodule];

    var entryE  = cE(contentE, "hbox", "entry");
    entryE.classList.add("click");
    entryE.textContent = gApp.strings.getLabelForType({ fullName : fullName }) ||
        capitalizeWords(typeNameReadable(fullName.replace("Summary", "")));

    entryE.addEventListener("click", event => openSearchForm(fullName), false);
  });
}

function loadAllResources(successCallback) {
  /*
  var modules = {};
  var types = gApp.getTypesList();
  gApp.getTypesList().forEach(fullName => {
    var nameParts = fullName.split(".");
    if (nameParts < 4) return;
    var moduleName = nameParts[0];
    modules[moduleName] = true;
  });
  for (var moduleName in modules) {
  */
  var waiting = 0;
  var serverReturned = () => {
    if (--waiting == 0) {
      successCallback();
    }
  };
  var modules = [ "Accounting", "Bethel", "Core", "DocumentManagement",
      "Education", "Facilities", "Field", "Persons", "Printery", "Purchasing" ];
  modules.forEach(moduleName => {
    waiting++;
    gApp.strings.getAsync(moduleName, serverReturned, ex => {
      errorNonCritical(ex);
      serverReturned();
    });
  });
}

function openSearchForm(entityFullName) {
  openTab("search.html#fullname=" + entityFullName);
}


/* Quick search field */

function buildQuickSearchField() {
  var inputE = E("quickSearchField");
  inputE.placeholder = "TypeName <space> simple search";
  var fromAC = false;
  hookupReturnKey(inputE, event => {
    if (fromAC) {
      fromAC = false;
      return;
    }
    startQuickSearch();
  });
  var autocomplete = new AutocompleteWidget(inputE, {
    defaultAction : item => {
      inputE.value = item.label + " ";
      fromAC = true;
    },
    onActionCopyText : false,
    delay : 50,
    parent : document.querySelector("spacer"),
    //parent : E("quickSearchBar"),
  });
  var source = new QuickSearchSource(autocomplete);
  autocomplete.addSource(source);

  inputE.addEventListener("keydown", event => {
    if (event.keyCode == 32) { // space
      if (autocomplete.isShowingSuggestions() && autocomplete.currentItem()) {
        autocomplete._defaultAction(autocomplete.currentItem());
        autocomplete.hideSuggestions();
        event.preventDefault();
      }
    }
  }, false);

  gSummaryNamesCache = gApp.getSummariesList().map(fullName => {
    var parts = fullName.split(".");
    return strRemoveEnd(parts[parts.length - 1], "Summary");
  });
}

function startQuickSearch() {
  var queryStr = E("quickSearchField").value;
  var spacePos = queryStr.indexOf(" ");
  var typeName = queryStr;
  var simpleSearch;
  if (spacePos > 0) {
    typeName = queryStr.substr(0, spacePos);
    simpleSearch = queryStr.substr(spacePos + 1);
  }
  if (!arrayContains(gSummaryNamesCache, typeName)) {
    errorCritical("Type " + typeName + " not found");
    return;
  }
  var fullNames = gApp.getSummariesList().filter(fullName => strEndsWith(fullName, "." + typeName + "Summary"));
  if (fullNames.length > 1) {
    errorCritical(typeName + " not found several times, so I can't do a quick search");
    return;
  }
  var fullName = fullNames[0];

  if (simpleSearch) {
    openTab("search.html#fullname=" + fullName + "&simpleSearch=" + encodeURIComponent(simpleSearch) + "&openDirectly=true");
  } else {
    openTab("search.html#fullname=" + fullName);
  }
}


/**
 * Autocompletes type names in the quick search field
 * @param ac {AutocompleteWidget}
 */
function QuickSearchSource(ac) {
  assert(ac instanceof AutocompleteWidget);
  this._ac = ac;
}
QuickSearchSource.prototype = {
  _ac : null,

  onTextChanged : function(text) {
    this._ac.hideSuggestions();
    this._ac.clearItems();
    if (!text) return;
    if (strContains(text, " ")) return;
    gSummaryNamesCache.filter(typeName => strContains(typeName.toLowerCase(), text.toLowerCase())).forEach(typeName => {
      this._ac.addItem(new SimpleAutocompleteItem(null, typeName));
    });
    this._ac.shouldShowSuggestions(); // opens dropdown
  },

  cancel : function() {
  },
};
extend(QuickSearchSource, AutocompleteSource);
