/**
 * Displays a concrete entity
 *
 * URL: #id=23&fullName=Facilities.PropertyManagement.Properties.Lease
 */

var gApp; // {Application} singleton -- will be set by page opener
//var gEntityInstance; // {EntityInstance} The entity to display. Once set, the object identity should not change, but its properties do. The global is currently not needed.
/**
 * {Map of property name without type name {String} -> field {DataField}}
 */
var gFields = {};

function onLoad() {
  getAppContext();
  var query = parseURLQueryString(window.location.hash.replace("#", ""));
  var fullName = sanitize.identifier(query.fullname);
  var id = sanitize.integer(query.id);
  setTitleFromFullName(fullName);
  loadEntityPage(fullName, id);
}

function loadEntityPage(fullName, id) {
  // Get all the data
  gApp.fetchInitialType(fullName + "Summary", () => { // HACK needed sometimes for enums. It's a null-op, if you were already on the search page.
    gApp.fetchInitialType(fullName, entityType => {
      // prefetch all resources for this module
      gApp.strings.fetchLabelsForType(entityType, () => {
        var entityInstance = entityType.newInstance(id);
        //gEntityInstance = entityInstance;
        setTitleFromEntity(entityInstance);
        buildEntityPage(entityInstance);
        if (id) {
          entityInstance.fetch(() => {
            // Fields update automatically thanks to change observers

            // Fetch 1:n lists
            if (entityType instanceof AggregateEntityType) {
              entityType.subListTypes.forEach(subListType => {
                entityInstance.fetchList(subListType, items => {
                  // Lists update automatically thanks to change observers
                }, errorCritical);
              });
            }
          }, errorCritical);
        } // else new record
      }, errorCritical);
    }, errorCritical);
  }, errorCritical);
}
window.addEventListener("DOMContentLoaded", onLoad, false);

function buildEntityPage(entityInstance) {
  if (entityInstance instanceof AggregateEntityInstance) {
    entityInstance.subInstances.forEach(subInstance => {
      buildSubEntity(subInstance);
    });
    entityInstance.entityType.subListTypes.forEach(subListType => {
      buildSubEntityList(entityInstance, subListType);
    });
  } else {
    buildSubEntity(entityInstance);
  }

  addButtons(entityInstance);
}

function buildSubEntity(entityInstance) {
  var entityType = entityInstance.entityType;
  var groupTitle = gApp.strings.getLabelForType(entityType);
  var group = createGroup(groupTitle);
  entityType.properties.forEach(propInfo => {
    var field = newFieldForProperty(entityInstance, propInfo);
    if (!field) {
      return; // TODO remove once we support all types
    }
    var label = gApp.strings.getLabelForProperty(entityType, propInfo.name);
    if (!label) {
      label = typeNameReadable(propInfo.name); // fallback, if we don't find a resource
    }
    var labeledField = new LabeledField(label, field);
    gFields[propInfo.name] = labeledField;
    group.append(labeledField.element);
  });
  E("entityPage").appendChild(group.element);
}

function buildSubEntityList(entityInstance, subListType) {
  var groupTitle = gApp.strings.getLabelForType(subListType);
  var group = createGroup(groupTitle);
  group.element.classList.add("grid");

  E("entityPage").appendChild(group.element);
  var grid;
  entityInstance.changeObservers.push(reason => { // TODO use observable collection instead
    if (grid) {
      return; // already populated, listener is being called for another reason.
    }
    var results = entityInstance.getSubListInstances(subListType);
    if (!results) {
      return; // not yet fetched
    }
    grid = new EntityGrid(subListType, results, true, true,
        selectedItems =>{});
    group.append(grid.getElement());
  });
}

function addButtons(entityInstance) {
  var globalE = E("footerGlobal");
  var newButtonE = createBigButton(globalE, "New", "icons/plus.png", event => {
    openTab("entity.html#fullname=" + entityInstance.entityType.fullName + "&id=0");
  }, errorCritical);
  var saveButtonE = createBigButton(globalE, "Save", "icons/save.png", event => {
    saveEntity(entityInstance);
  }, errorCritical);
  var undoButtonE = createBigButton(globalE, "Undo", "icons/undo.png", event => {
    entityInstance.undo();
  }, errorCritical);
  entityInstance.changeObservers.push(reason => {
    if (entityInstance.isDirty()) {
      saveButtonE.removeAttribute("disabled");
      undoButtonE.removeAttribute("disabled");
    } else {
      saveButtonE.setAttribute("disabled", "disabled");
      undoButtonE.setAttribute("disabled", "disabled");
    }
  });
  saveButtonE.setAttribute("disabled", "disabled"); // default state
  undoButtonE.setAttribute("disabled", "disabled");
}


function saveEntity(entityInstance) {
  entityInstance.save(() => {
    entityInstance.fetch(() => { // In case server changed something, e.g. added IDs
      // Change listeners automatically update the UI
    });
  }, ex => { // error handling
    var shown = false;
    if (ex instanceof JasminException) {
      ex.errors.forEach(error => {
        var field = gFields[error.fieldName];
        if (field) {
          field.setErrorMessage(error.message);
        } else {
          errorCritical(error.message);
          // TODO acculumate them nicer
        }
        shown = true;
      });
    }
    if (!shown) {
      errorCritical(ex);
    }
  });
}


/**
 * Call this when we loaded the type, the instance and all resources.
 * You need to call this only once, because it will install a change observer on the instance.
 * @param entityInstance {EntityInstance}
 */
function setTitleFromEntity(entityInstance) {
  assert(entityInstance instanceof EntityInstance);
  var typeLabel = gApp.strings.getLabelForType(entityInstance.entityType);
  var instanceTitle = entityInstance.id ? entityInstance.title() : "(new)";
  document.title = (instanceTitle ? instanceTitle  + " - ": "") + typeLabel + " - " + gApp.strings.appName;
  E("title").textContent = typeLabel + (instanceTitle ? ": " + instanceTitle : "");
  entityInstance.changeObservers.push(reason => {
    setTitleFromEntity(entityInstance);
  });
}

/**
 * Just a quick default, before the server fetches.
 * @param fullName {String}
 */
function setTitleFromFullName(fullName) {
  sanitize.label(fullName);
  var typeLabel = typeNameReadable(fullName);
  document.title = typeLabel + " - " + gApp.strings.appName;
  E("title").textContent = typeLabel;
}




/**
 * @param label {String} user-readable, translated string that describes the field
 * @param field {DataField}
 * @returns {
 *   append : {Function(newElement {DOMElement}},
 *   element : {DOMElement},
 * }
 */
function createGroup(title) {
  var boxE = cE(null, "vbox", "group");
  var titleE = cE(boxE, "div", "groupTitle");
  titleE.textContent = title;
  var wrappingE = cE(boxE, "div", "groupContent");
  var appendFunc = newElement => wrappingE.appendChild(newElement);
  return {
    append : appendFunc,
    element : boxE,
  };
}
