/**
 * Contains misc UI functions that are needed across the app.
 * These here are specific to Jasmin.
 *
 * In constrast, uiutil.js contains generic UI util functions that are generic
 * and can be used in other project, e.g. about DOM access.
 */


/**
 * Opens an app page in a new top-level browser tab.
 * Copies the app context to the new page.
 *
 * Attention popup blocker: Do not use async processing between the onclick
 * handler and the call to openTab() = window.open().
 * If you do you use callbacks, the popup blocker will trigger.
 * When you hit window.open(), Firefox must find the click event handler
 * in the call stack. If it does, Firefox generally allows the new window.
 *
 * @returns {DOMWindow}
 */
function openTab(url) {
  return window.open(gApp.baseURL + url);
}

/**
 * Allows a page that has been opened via openTab() to recover the
 * gApp instance and model from the other page,
 * instead of reloading everything.
 */
function getAppContext() {
  var parentWindow = window.opener;
  if (!parentWindow) {
    alert("The Jasmin start page has gone away. You need to re-open this page from another Jasmin page.");
    throw new UserError("No parent window. Need app context.");
  }
  gApp = parentWindow.gApp;
  for (var name in parentWindow.modelExports) {
    window[name] = parentWindow.modelExports[name];
  }
  window.modelExports = parentWindow.modelExports;
}


/* Error UI */

/**
 * Show error message to end user.
 * This should include all errors that are in response to actions that
 * the user requested immediately before, and that block the proper
 * execution of that action.
 * It should not be used for code that is automatic (on startup, on timer).
 * If in doubt, we assume the error is fatal.
 *
 * Current implementation shows a dialog to the end user.
 *
 * @param e {Exception or String}   the error message
 */
function errorCritical(e)
{
  _debugError(e);
  alert(e);
};

/**
 * Minor errors.
 * This should include all errors in code that is automatic (on startup, on timer).
 * Also any errors that do not block the main purpose of the action.
 *
 * The current implementation does not show the error to the
 * end user at all, but only on the error console.
 * Future implementations may show it in a non-obtrusive way, e.g.
 * as an icon somewhere.
 *
 * If in doubt, use errorCritical(), not errorNonCritical().
 *
 * @param e {Exception or String}   the error message
 * @param parentWindow {window} (Optional)
 */
function errorNonCritical(e, parentWindow)
{
  _debugError(e);
};

function _debugError(e)
{
  ddebug("ERROR: " + e);
  ddebug("Stack:\n" + (e.stack ? e.stack : "none"));
};



/**
 * Encodes naming conventions in Rin project
 * and allows to infer and convert information.
 */

/**
 * Inserts spaces and fixes casing for CamelCase,
 * e.g. "CamelCase" -> "Camel case"
 *
 * This is generally only a workaround for a missing Resources.resx string.
 */
function typeNameReadable(fullName) {
  var typeName = fullName.split(".").pop();
  typeName = strRemoveEnd(typeName, "Summary");
  typeName = strRemoveEnd(typeName, "Id");
  if (strStartsWith(typeName, "Is")) {
    typeName = typeName.substr(2);
  }
  return capitalize(camelCaseToWords(typeName).join(" "));
}

/**
 * @param word {String} e.g. "this is a word"
 * @return e.g. "This is a word"
 */
function capitalize(word) {
  return word[0].toUpperCase() + word.substr(1);
}

/**
 * @param phrase {String} e.g. "this is a phrase"
 * @return e.g. "This Is A Phrase"
 */
function capitalizeWords(phrase) {
  //return phrase;
  var words = phrase.split(" ");
  var result = "";
  words.forEach(word => result += " " + capitalize(word));
  return result.trim();
}

/**
 * @param camelCase {String} e.g. "ThisIsSomePhraseAsCamelCase"
 * @return words {Array of {String}} e.g. ["this", "is", "some", "phrase", "as", "camel", "case"]
 */
function camelCaseToWords(camelCase) {
  var result = [];
  var word = "";
  for (var i = 0; i < camelCase.length; i++) {
    var ch = camelCase[i];
    if (ch == ch.toUpperCase()) {
      if (word) {
        result.push(word);
      }
      word = ch.toLocaleLowerCase();
    } else {
      word += ch;
    }
  }
  if (word) {
    result.push(word);
  }
  return result;
}
