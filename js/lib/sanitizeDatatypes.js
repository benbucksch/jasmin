/**
 * This is a generic input validation lib. Use it when you process
 * data from the network.
 *
 * Just a few functions which verify, for security purposes, that the
 * input variables (strings, if nothing else is noted) are of the expected
 * type and syntax.
 *
 * The functions take a string (unless noted otherwise) and return
 * the expected datatype in JS types. If the value is not as expected,
 * they throw exceptions.
 */

var sanitize =
{
  integer : function(unchecked)
  {
    if (typeof(unchecked) == "number")
      return unchecked;

    var r = parseInt(unchecked);
    if (isNaN(r))
      throw new MalformedException("no_number.error", unchecked);

    return r;
  },

  integerRange : function(unchecked, min, max)
  {
    var i = this.integer(unchecked);
    if (i < min)
      throw new MalformedException("number_too_small.error", unchecked);

    if (i > max)
      throw new MalformedException("number_too_large.error", unchecked);

    return i;
  },

  boolean : function(unchecked)
  {
    if (typeof(unchecked) == "boolean")
      return unchecked;

    if (unchecked == "true")
      return true;

    if (unchecked == "false")
      return false;

    throw new MalformedException("boolean.error", unchecked);
  },

  string : function(unchecked)
  {
    return String(unchecked);
  },

  nonemptystring : function(unchecked)
  {
    if (!unchecked)
      throw new MalformedException("string_empty.error", unchecked);

    return this.string(unchecked);
  },

  /**
   * Allow only letters, numbers, "-" and "_".
   *
   * Empty strings not allowed (good idea?).
   */
  alphanumdash : function(unchecked)
  {
    var str = this.nonemptystring(unchecked);
    if (!/^[a-zA-Z0-9\-\_]*$/.test(str))
      throw new MalformedException("alphanumdash.error", unchecked);

    return str;
  },

  /**
   * Allow only common code identifiers: letters, numbers, "_" and ".".
   *
   * Empty strings not allowed.
   */
  identifier : function(unchecked)
  {
    var str = this.nonemptystring(unchecked);
    if (!/^[a-zA-Z0-9\.\_]*$/.test(str))
      throw new MalformedException("identifier.error", unchecked);
    return str;
  },

  /**
   * DNS hostnames like foo.bar.example.com
   * Allow only letters, numbers, "-" and "."
   * Empty strings not allowed.
   * Currently does not support IDN (international domain names).
   * HACK: "%" is allowed, because we allow placeholders in hostnames in the
   * config file.
   */
  hostname : function(unchecked)
  {
    var str = this.nonemptystring(unchecked);
    if (!/^[a-zA-Z0-9\-\.%]*$/.test(unchecked))
      throw new MalformedException("hostname_syntax.error", unchecked);

    return str.toLowerCase();
  },

  /**
   * A non-chrome URL that's safe to request.
   */
  url : function (unchecked)
  {
    var str =  this.string(unchecked);
    if (str.substr(0, 5) != "http:" && str.substr(0, 6) != "https:" &&
        str.substr(0, 4) != "ftp:")
      throw new MalformedException("url_scheme.error", unchecked);

    //TODO security-check URL

    return str;
  },

  /**
   * A value which should be shown to the user in the UI as label
   */
  label : function(unchecked)
  {
    return this.string(unchecked);
  },

  /**
   * Allows only certain values as input, otherwise throw.
   *
   * @param unchecked {Any} The value to check
   * @param allowedValues {Array} List of values that |unchecked| may have.
   * @param defaultValue {Any} (Optional) If |unchecked| does not match
   *       anything in |mapping|, a |defaultValue| can be returned instead of
   *       throwing an exception. The latter is the default and happens when
   *       no |defaultValue| is passed.
   * @throws MalformedException
   */
  enum : function(unchecked, allowedValues, defaultValue)
  {
    var checkedValue = allowedValues.filter(function(allowedValue) {
      return allowedValue == unchecked;
    })[0];
    if (checkedValue) {
      return checkedValue;
    }
    // value is bad
    if (typeof(defaultValue) == "undefined")
      throw new MalformedException("allowed_value.error", unchecked);
    return defaultValue;
  },

  /**
   * Like enum, allows only certain (string) values as input, but allows the
   * caller to specify another value to return instead of the input value. E.g.,
   * if unchecked == "foo", return 1, if unchecked == "bar", return 2,
   * otherwise throw. This allows to translate string enums into integer enums.
   *
   * @param unchecked {Any} The value to check
   * @param mapping {Object} Associative array. property name is the input
   *       value, property value is the output value. E.g. the example above
   *       would be: { foo: 1, bar : 2 }.
   *       Use quotes when you need freaky characters: "baz-" : 3.
   * @param defaultValue {Any} (Optional) If |unchecked| does not match
   *       anything in |mapping|, a |defaultValue| can be returned instead of
   *       throwing an exception. The latter is the default and happens when
   *       no |defaultValue| is passed.
   * @throws MalformedException
   */
  translate : function(unchecked, mapping, defaultValue)
  {
    for (var inputValue in mapping)
    {
      if (inputValue == unchecked)
        return mapping[inputValue];
    }
    // value is bad
    if (typeof(defaultValue) == "undefined")
      throw new MalformedException("allowed_value.error", unchecked);
    return defaultValue;
  }
};

function MalformedException(msgID, uncheckedBadValue)
{
  //var msg = gStringBundle.get(msgID); TODO l10n
  var msg = "Error while checking " + strReplaceAll(msgID, "_", " ").replace(".error", "");
  ddebug(msg += " (bad value: " + new String(uncheckedBadValue) + ")");
  Exception.call(this, msg);
}
extend(MalformedException, Exception);
