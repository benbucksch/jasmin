/**
 * This is a small wrapper around XMLHttpRequest, which solves various
 * inadequacies of the API, e.g. error handling. It is entirely generic.
 *
 * It does not provide download progress, but assumes that the
 * fetched resource is so small (<1 10 KB) that the roundtrip and
 * response generation is far more significant than the
 * download time of the response. In other words, it's fine for RPC,
 * but not for bigger file downloads.
 */

/**
 * Set up a fetch.
 *
 * All of the following parameters (apart from the callbacks) are passed
 * as object, with the property name matching the parameter name listed
 * below.
 * @param url {String}   URL of the server function.
 *    ATTENTION: The caller needs to make sure that the URL is secure to call.
 * @param urlArgs {Object, associative array} Parameters to add
 *   to the end of the URL as query string. E.g.
 *   { foo: "bla", bar: "blub blub" } will add "?foo=bla&bar=blub%20blub"
 *   to the URL
 *   (unless the URL already has a "?", then it adds "&foo...").
 *   The values will be encoded as needed, so pass them unencoded.
 * @param headers {Object, associative array} Extra headers
 *   that will be added as HTTP headers.
 *   { foo: "blub blub" } will add "Foo: Blub blub"
 *   The values will *not* be encoded, so pass them encoded if needed.
 * @param bodyFormArgs {Object, associative array} Like urlArgs,
 *   just that the params will be sent x-www-form-urlencoded
 *   in the body, like a HTML form post.
 *   The values will be urlComponentEncoded, so pass them unencoded.
 *   This cannot be used together with |uploadBody|.
 * @param uploadBody {Object}   Arbitrary object, which to use as
 *   body of the HTTP request. Will also set the mimetype accordingly.
 *   Only supported object types, currently only JSON and plaintext is supported.
 *   Usually, when you have nothing to upload, just pass |null| or |undefined|.
 * @param method {String-enum}   HTTP method to use.
 *   "GET", "POST" or "DELETE"
 *   Only influences the HTTP request method,
 *   i.e. first line of the HTTP request, not the body or parameters.
 *   Use POST when you modify server state,
 *   GET when you only request information.
 *
 * @param successCallback {Function(result {String})}
 *   Called when the server call worked (no errors).
 *   |result| will contain the body of the HTTP reponse, as string.
 * @param errorCallback {Function(ex)}
 *   Called in case of error. ex contains the error
 *   with a user-displayable but not localized |.message| and maybe a
 *   |.code|, which can be either
 *  - an nsresult error code,
 *  - an HTTP result error code (0...1000) or
 *  - negative: 0...-100 :
 *     -2 = can't resolve server in DNS etc.
 *     -4 = response body (e.g. XML) malformed
 */
function FetchHTTP(args, successCallback, errorCallback)
{
  assert(typeof(successCallback) == "function", "BUG: need successCallback");
  assert(typeof(errorCallback) == "function", "BUG: need errorCallback");
  assert(!(args.bodyFormArgs && args.uploadBody),
      "Cannot combine bodyFormArgs with uploadBody");
  this._url = sanitize.string(args.url);
  this._method = sanitize.enum(args.method, ["GET", "POST", "HEAD", "DELETE"], "GET");
  this._urlArgs = args.urlArgs ? args.urlArgs : {};
  this._bodyFormArgs = args.bodyFormArgs ? args.bodyFormArgs : null;
  this._headers = args.headers ? args.headers : {};
  this._uploadBody = args.uploadBody;
  this._successCallback = successCallback;
  this._errorCallback = errorCallback;
}
FetchHTTP.prototype =
{
  _url : null, // URL as passed to ctor, without arguments
  _urlArgs : null,
  _headers : null,
  _bodyFormArgs : null,
  _uploadBody : null,
  _post : null,
  _successCallback : null,
  _errorCallback : null,
  _request : null, // the XMLHttpRequest object
  _callStack : null, // The original call stack
  result : null,

  start : function()
  {
    // URL
    var url = this._url;
    for (var name in this._urlArgs)
    {
      url += (url.indexOf("?") == -1 ? "?" : "&") +
              name + "=" + encodeURIComponent(this._urlArgs[name]);
    }

    // body
    var mimetype = null;
    var body = null;
    if (this._uploadBody)
    {
      if (typeof(this._uploadBody) == "string")
      {
        mimetype = "text/plain; charset=UTF-8";
        body = this._uploadBody;
      }
      else if (typeof(this._uploadBody) == "object")
      {
        mimetype = "text/json; charset=UTF-8";
        body = JSON.stringify(this._uploadBody);
      }
    }
    else if (this._bodyFormArgs)
    {
      mimetype = "application/x-www-form-urlencoded; charset=UTF-8";
      body = "";
      for (var name in this._bodyFormArgs)
      {
        body += (body ? "&" : "") + name + "=" +
            encodeURIComponent(this._bodyFormArgs[name]);
      }
    }

    // Fire
    this._request = new XMLHttpRequest();
    var request = this._request;
    ddebug("contacting <" + url + ">");
    request.open(this._method, url);

    // Headers
    if (mimetype)
      request.setRequestHeader("Content-Type", mimetype + "; charset=UTF-8");
    for (var name in this._headers)
    {
      request.setRequestHeader(name, this._headers[name]);
      if (name == "Cookie")
      {
        // TODO
      }
    }

    var me = this;
    request.onload = function() { me._response(true); }
    request.onerror = function() { me._response(false); }
    request.send(body);
    // Store the original stack so we can use it if there is an exception
    this._callStack = Error().stack;
  },
  _response : function(success, exStored)
  {
    try
    {
    var errorCode = null;
    var errorStr = null;

    try
    {
      // response
      var mimetype = this._request.getResponseHeader("Content-Type");
      //console.log("response for <" + this._url + ">: " + mimetype + " " +  this._request.responseText);
      if (!mimetype)
        mimetype = "";
      mimetype = mimetype.split(";")[0];
      if (mimetype == "text/xml" ||
          mimetype == "application/xml" ||
          mimetype == "text/rdf" ||
          // Match application/foo+xml
          /^application\/[a-z0-9]+\+xml$/.test(mimetype) ||
          // Match text/foo+xml
          /^text\/[a-z0-9]+\+xml$/.test(mimetype))
      {
        this.result = this._request.responseXML;
      }
      else if (mimetype == "application/json")
      {
        this.result = JSON.parse(this._request.responseText);
      }
      else
      {
        this.result = this._request.responseText;
      }
    }
    catch (e)
    {
      success = false;
      errorCode = -4;
      //errorStr = gStringBundle.get("bad_response_content.error") + ": " + e; TODO l10n
      errorStr = "Bad response from server: " + e;
      // in case of normal error code, above errorCode/Str will be overwritten below.
    }

    if (success && this._request.status >= 200 &&
        this._request.status < 300) // HTTP level success
    {
      // go on
    }
    else
    {
      success = false;
      try
      {
        errorCode = this._request.status;
        errorStr = this._request.statusText;
      } catch (e) {
        // If we can't resolve the hostname in DNS etc.,
        // .statusText throws in FF3.6, while FF 4+ gives .statusText = "".
        // Both versions give success = false and status = 0.
        errorCode = 0;
        errorStr = "";
      }
      if (errorCode == 0 && errorStr == "")
      {
        errorCode = -2;
        //errorStr = gStringBundle.get("cannot_contact_server.error"); TODO l10n
        errorStr = "Cannot contact server";
        try {
          // try to get a more precise error from nsIHttpChannel / nsIRequest
          errorCode = this._request.channel.status; // gives an nsresult
          errorStr = errorStr + "\n("+ getErrorText(errorCode) +")"; //util.js
        } catch (e) {} // ignore, we already have set a default above
      }
    }

    // Callbacks
    try {
      if (success)
        this._successCallback(this.result);
      else if (exStored)
        this._errorCallback(exStored);
      else {
        // Put the caller's stack into the exception
        var ex = new ServerException(errorStr, errorCode, this._url);
        ex.stack = this._callStack;
        ex.resultBody = this.result;
        this._errorCallback(ex);
      }

      if (this._finishedCallback)
        this._finishedCallback(this);
    } catch (e) { this._errorInCallback(e); }

    } catch (e) {
      // error in our fetchhttp._response() code
      errorInBackend(e);
      throw(e); // to error console
    }
  },
  _errorInCallback : function(e)
  {
    // error in callback or our fetchhttp._response() code
    try {
      this._errorCallback(e);
    } catch (e) {
      errorInBackend(e); // error in errorCallback, too!
      throw(e); // to error console
    }
  },
  /**
   * Call this between start() and finishedCallback fired.
   */
  cancel : function(ex)
  {
    // TODO must able able to call cancel() at any time, without errors

    this._request.abort();

    // Need to manually call error handler
    // <https://bugzilla.mozilla.org/show_bug.cgi?id=218236#c11>
    this._response(false, ex ? ex : new UserCancelledException());
  },
  /**
   * Allows caller or lib to be notified when the call is done.
   * This is useful to enable and disable a Cancel button in the UI,
   * which allows to cancel the network request.
   */
  setFinishedCallback : function(finishedCallback)
  {
    this._finishedCallback = finishedCallback;
  },
  /**
   * Gets an HTTP header value that the server sent.
   * @param headername {String}
   * @returns {String}   value of the header
   *    or null if
   *      - called before server responsed
   *      - header does not exist in response
   */
  getResponseHeader : function(headername)
  {
    return this._request.getResponseHeader(headername);
  },
  /**
   * Gets the text of the HTTP response.
   * @returns {String}   response body as plaintext.
   *    null if called before server responded
   */
  getResponseText : function()
  {
    return this._request.responseText;
  },
}
extend(FetchHTTP, Abortable);

/**
 * This is an alternative implementation of FetchHTTP,
 * purely for testing purposes, which does not make any
 * network connection, but takes a hardcoded response
 * from the caller and feeds it to the caller's callbacks.
 * You can set either a success (optionally with a body) or
 * an error with error code and msg. You can also set
 * the response headers. It's totally stupid and all hardcoded,
 * really just for testing.
 */
function FakeFetchHTTP()
{
  FetchHTTP.apply(this, arguments);
}
FakeFetchHTTP.prototype =
{
  _responseHeaders : {},
  getResponseHeader : function(headername)
  {
    return this._responseHeaders[headername];
  },
  /**
   * Call this instead of start(), to call the successCallback
   * @param response {XML or String or null) will be passed verbatim
   *     as parameter to successCallback()
   * @param responseHeaders {Object} (optional)
   */
  setSuccessResponse : function(response, responseHeaders)
  {
    if (responseHeaders)
      this._responseHeaders = responseHeaders;
    try {
      this._successCallback(response);
      if (this._finishedCallback)
        this._finishedCallback(this);
    } catch (e) {
      this._errorInCallback(e);
    }
  },
  /**
   * Call this instead of start(), to call the errorCallback
   * @param httpCode {Integer} HTTP error code (e.g. 404)
   * @param httpStatusText {String} The line after the HTTP error code,
   *     e.g. "Not Found"
   * @param responseHeaders {Object} (optional)
   */
  setErrorResponse : function(httpCode, httpStatusText, responseHeaders)
  {
    assert(httpCode >= 400 && httpStatusText);
    if (responseHeaders)
      this._responseHeaders = responseHeaders;
    try {
      this._errorCallback(new ServerException(httpStatusText, httpCode, this._url));
      if (this._finishedCallback)
        this._finishedCallback(this);
    } catch (e) {
      this._errorInCallback(e);
    }
  },
  start : function()
  {
    throw NotReached("You should call setSuccess/ErrorResponse()");
  },
}


function UserCancelledException(msg)
{
  // The user knows they cancelled so I don't see a need
  // for a message to that effect.
  if (!msg)
    msg = "";
  this.causedByUser = true;
  Exception.call(this, msg);
}
UserCancelledException.prototype =
{
}
extend(UserCancelledException, Exception);

function ServerException(serverMsg, code, uri)
{
  var msg = serverMsg;
  if (code >= 300 && code < 600) { // HTTP error code
    msg += " " + code;
  }
  msg += " <" + uri + ">";
  Exception.call(this, msg);
  this.rootErrorMsg = serverMsg;
  this.code = code;
  this.uri = uri;
}
ServerException.prototype =
{
}
extend(ServerException, Exception);
