/**
 * Shortcut for document.getElementById()
 * @param id {String} Element ID
 * @returns {DOMElement} Either the element or null if no element was found
 */
function E(id) {
  return document.getElementById(id);
}

/**
 * createElement()
 * @param parentE {DOMElement} append the new element to this parent element
 * @param tagname {String} <tagname>
 * @param classname {String} class="classname"
 * @param attributes {Map of attribute name {String} -> attribute content {String}}
 */
function cE(parentE, tagname, classname, attributes) {
  var el = document.createElement(tagname);
  if (classname)
    el.classList.add(classname);
  for (var name in attributes)
    el.setAttribute(name, attributes[name]);
  if (parentE) {
    parentE.appendChild(el);
  }
  return el;
}

/**
 * createTextNode()
 */
function cTN(text) {
  return document.createTextNode(text);
}

/**
 * Like parentElement.insertBefore(newElement, insertBefore), just insert
 * after some other element.
 *
 * @param parentElement {node} Insert |newElement| as child of |parentElement|.
 * @param newElement {node} new node that you want to insert
 * @param insertAfterInfo {String or DOMElement}  Element or ID of the node
 *     that should be before (left to) |newElement|.
 *     This must be a child of |parentElement|.
 *     If it does not exist, the |newElement| is added to the end.
 * @returns {node} the node that was inserted
 */
function insertAfter(parentElement, newElement, insertAfterInfo) {
  var afterEl = null;
  if (insertAfterInfo) {
    if (typeof(insertAfterInfo) == "string") {
      afterEl = parentElement.ownerDocument.getElementById(insertAfterInfo);
    } else if (insertAfterInfo.ownerDocument) {
      afterEl = insertAfterInfo;
    } else {
      throw new NotReached("insertAfterInfo has the wrong type");
    }
    if (afterEl.parentNode != parentElement) {
      throw new NotReached("insertAfterInfo has the wrong parent element");
    }
  }
  if (afterEl && afterEl.nextSibling) {
    parentElement.insertBefore(newElement, afterEl.nextSibling);
  } else {
    parentElement.appendChild(newElement);
  }
  return newElement;
}

/**
 * Turns a DOM |NodeList| into a JS array that you can |for each| on
 * - convenience
 * - makes a copy, which is needed when you remove the elements
 */
function nodeListToArray(nodeList)
{
  var result = [];
  for (var i = 0, l = nodeList.length; i < l; i++)
    result.push(nodeList.item(i));
  return result;
}

function querySelectorAll(rootE, selector) {
  if (selector[0] != ">") {
    return nodeListToArray(rootE.querySelectorAll(selector));

  } else { // when "> Foo", then only direct <Foo> children
    var tagName = selector.substr(1).trim();
    var result = [];
    nodeListToArray(rootE.childNodes).forEach(node => {
      if (node.localName != tagName) {
        return;
      }
      result.push(node);
    });
    return result;
  }
}

function cleanElement(el)
{
  while (el.hasChildNodes())
    el.removeChild(el.firstChild);
}

function removeElement(el)
{
  if (el && el.parentNode) {
    el.parentNode.removeChild(el);
  }
}

/**
 * For a given element, finds the tagname which contains it
 * @param element {DOMElement}
 * @return {DOMElement <tagname>} or null
 */
function findParentTagForElement(tagname, element)
{
  assert(element && element instanceof Ci.nsIDOMElement);
  sanitize.nonemptystring(tagname);
  for (; element && element.tagName != tagname; element = element.parentNode)
    ;
  return element;
}



/**
 * Helper function that handles detecting the return key in a
 * web page and calls the appropriate function
 */
function hookupReturnKey(element, onReturnKeypressCallback) {
  element.addEventListener("keyup", function(event) {
    if (event.keyCode == 13) {
      onReturnKeypressCallback(event);
    }
  }, false);
}


/**
 * Utility function that gets all elements that have a
 * translate="" atttribute, uses it as stringbundle key,
 * gets the corresponding string from the |stringbundle|,
 * and sets the element content textnode to that string.
 *
 * @param container {DOMElement}   Iterators over this element
 * @param stringbundle {StringBundle}
 * @param brand {String} brand to use for substitution in strings
 */
function translateElements(container, stringbundle, placeholders) {
  nodeListToArray(container.querySelectorAll("*[translate]")).forEach(function(el) {
    var label = stringbundle.get(el.getAttribute("translate"));
    for (var placeholder in placeholders) {
      label = strReplaceAll(label, "%" + placeholder + "%", placeholders[placeholder]);
    }
    el.appendChild(cTN(label));
  });

  nodeListToArray(container.querySelectorAll("*[translate-attr]")).forEach(function(el) {
    el.getAttribute("translate-attr").split(",").forEach(function(attrNameValue) {
      var attrSplit = attrNameValue.split("=");
      var attrName = attrSplit[0].trim();
      var attrValue = attrSplit[1].trim();
      var label = stringbundle.get(attrValue);
      for (var placeholder in placeholders) {
        label = strReplaceAll(label, "%" + placeholder + "%", placeholders[placeholder]);
      }
      el.setAttribute(attrName, label);
    });
  });
}
